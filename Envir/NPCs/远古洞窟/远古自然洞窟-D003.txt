;;3F
;;Quest 153
[@MAIN]
#IF
CHECKQUEST 153 1
#SAY
带有古代符号的神秘石头。
"远古地牢 <Ones/@omacavea>" Level 10~22
 
<Close/@exit>
#ELSEACT
GOTO @MAIN-1
#ELSESAY
带有未知符号的神秘石头。

[@MAIN-1]
#IF
CHECK [535] 1
#ACT
SET [536] 1
#ELSESAY
A Mysterious Stone with Unknown symbols.
 
 
<Close/@exit>

[@omacavea]
#IF
LEVEL > 9 
LEVEL < 22
#ACT
MOVE D001A
#ELSESAY
Nothing happens.

[Quests]
152
-152
153
-153