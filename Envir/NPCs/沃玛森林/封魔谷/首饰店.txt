[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
欢迎光临，我能为您做些什么?
 
<查看/@BuySell> 商品.
<修理/@Repair> 首饰.
 
<Close/@exit>

[@BuySell]
#SAY
你想买或卖哪个项目?
<回购/@BuyBack>
 
<Back/@main>

[@BuyBack]
#SAY
这些都是还可以买回来的东西。
 
<Back/@main>

[@Repair]
#SAY
你可以修理各种各样的首饰。
  
<Back/@main>

[Types]
5
6
7

[Trade]
魔鬼项链
凤凰明珠
琥珀项链
灯笼项链
牛角戒指
生铁戒指
白玉戒指
蓝色水晶戒指
黑色水晶戒指
蛇眼戒指
珍珠戒指
金戒指
钢手镯
大手镯
尽力手镯
道士手镯
黑檀手镯
