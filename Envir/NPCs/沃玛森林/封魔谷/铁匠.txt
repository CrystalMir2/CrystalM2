[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
您好，您在找什么特别的东西吗?
我能帮上什么忙吗?
 
<查看/@BuySell> 商品.
<修理/@Repair> 武器.
 
<Close/@exit>

[@BuySell]
#SAY
你想买或卖哪个项目?
<回购/@BuyBack>
 
<Back/@main>

[@BuyBack]
#SAY
这些都是还可以买回来的东西。
 
<Back/@main>

[@Repair]
#SAY
你想修理武器吗?
给我看看需要它的武器。
 
<Back/@main>


[Types]
1
14

[Trade]
木剑
木弓
虎牙刀
匕首
乌木剑
乌木弓
青铜剑
暴虎刀
短剑
短弓
铁剑
骨弓
青铜斧
八荒
海魂
半月
音速刀
复合弓
鹤嘴锄