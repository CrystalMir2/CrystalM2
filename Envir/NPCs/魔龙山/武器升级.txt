[@MAIN]
#SAY
我是手工艺女士，专门在创造新的项目。
我被派来检查人们的生活条件。
不幸的是，似乎许多人仍在遭受贫困之苦。所以，我想用我的制作技能来帮助他们。
如果你还想做点什么，不妨问我。
<制作/@Craft>

[@Craft]
太阳水

[@BuySell]
#SAY
请选择您想购买或出售的配方。
<Buy Back/@BuyBack>

[RECIPE]
太阳水
勇猛神珠
魔性神珠
仙界神珠

[Quests]
34
134