[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
您好，您在找什么特别的东西吗?
我能帮上什么忙吗?
 
<查看/@BuySell> 商品.
<修理/@Repair> 武器.
 
<Close/@exit>

[@BuySell]
#SAY
你想买或卖哪个项目?
<回购/@BuyBack>
 
<Back/@main>

[@BuyBack]
#SAY
这些都是还可以买回来的东西。
 
<Back/@main>

[@Repair]
#SAY
你想修理武器吗?
给我看看需要它的武器。
 
<Back/@main>


[Types]
1
14

[Trade]
木剑
木弓
虎牙刀
匕首
乌木剑
乌木弓
青铜剑
暴虎刀
短剑
短弓
铁剑
骨弓
青铜斧
八荒
海魂
半月
音速刀
复合弓
鹤嘴锄


[Quests]
63
-64

[@talk]
#IF
CHECK [539] 1
#ACT
GOTO @MAIN1-1
#ELSEACT
GOTO @MAIN1-2

[@MAIN1-1]
#SAY
你好再次旅行. .今天天气这么好，你还好吗?
 
<Close/@exit>

[@MAIN1-2]
#IF
CHECKQUEST 155 1
#SAY
你已经向我证明了自己。
我会通知他们的 <听/@listen>.
<Close/@exit>
#ELSESAY
我怎么能相信你?我都不知道你是谁。
也许你能帮我个忙?
 
<Close/@exit>

[@listen]
#ACT
SET [539] 1
#SAY
告别旅行。
 
<Close/@exit>

