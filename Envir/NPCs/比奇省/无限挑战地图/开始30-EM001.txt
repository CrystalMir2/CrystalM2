[@Main]
#IF
CHECK [500] 0
#ACT
GOTO @check0
#ELSEACT
GOTO @done
[@done]
#IF
CHECK [500] 1
#ACT
GOTO @done2
#ELSEACT
GOTO @next1
[@done2]
#SAY
你已经完成了这个任务!
<Exit/@exit>
[@next1]
#IF
CHECK [501] 1
#ACT
GOTO @check1
#ELSEACT
GOTO @next2
[@next2]
#IF
CHECK [502] 1
#ACT
GOTO @check2
#ELSEACT
GOTO @next3
[@next3]
#IF
CHECK [503] 1
#ACT
GOTO @check3
#ELSEACT
GOTO @next4
[@next4]
#IF
CHECK [504] 1
#ACT
GOTO @check4
#ELSEACT
GOTO @next5
[@next5]
#IF
CHECK [505] 1
#ACT
GOTO @check5
#ELSEACT
GOTO @next6
[@next6]
#IF
CHECK [506] 1
#ACT
GOTO @check6
#ELSEACT
GOTO @next7
[@next7]
#IF
CHECK [507] 1
#ACT
GOTO @check7
#ELSEACT
GOTO @next8
[@next8]
#IF
CHECK [508] 1
#ACT
GOTO @check8
#ELSEACT
GOTO @next9
[@next9]
#IF
CHECK [509] 1
#ACT
GOTO @check9
#ELSEACT
GOTO @next10
[@next10]
#IF
CHECK [510] 1
#ACT
GOTO @check10
#ELSEACT
GOTO @next11
[@next11]
#IF
CHECK [511] 1
#ACT
GOTO @check11
#ELSEACT
GOTO @next12
[@next12]
#IF
CHECK [512] 1
#ACT
GOTO @check12
#ELSEACT
GOTO @next13
[@next13]
#IF
CHECK [513] 1
#ACT
GOTO @check13
#ELSEACT
GOTO @next14
[@next14]
#IF
CHECK [514] 1
#ACT
GOTO @check14
#ELSEACT
GOTO @next15
[@next15]
#IF
CHECK [515] 1
#ACT
GOTO @check15
#ELSEACT
GOTO @next16
[@next15]
#IF
CHECK [516] 1
#ACT
GOTO @check16
#ELSEACT
GOTO @next17
[@next17]
#IF
CHECK [517] 1
#ACT
GOTO @check17
#ELSEACT
GOTO @next18
[@next18]
#IF
CHECK [518] 1
#ACT
GOTO @check18
#ELSEACT
GOTO @next19
[@next19]
#IF
CHECK [519] 1
#ACT
GOTO @check19
#ELSEACT
GOTO @next20
[@next20]
#IF
CHECK [520] 1
#ACT
GOTO @check20

[@check0]
#IF
CHECK [501] 1
#ACT
GOTO @check1
#ELSEACT
GOTO @next21
[@next21]
#IF
CHECK [502] 1
#ACT
GOTO @check2
#ELSEACT
GOTO @next31
[@next31]
#IF
CHECK [503] 1
#ACT
GOTO @check3
#ELSEACT
GOTO @next41
[@next41]
#IF
CHECK [504] 1
#ACT
GOTO @check4
#ELSEACT
GOTO @next51
[@next51]
#IF
CHECK [505] 1
#ACT
GOTO @check5
#ELSEACT
GOTO @next61
[@next61]
#IF
CHECK [506] 1
#ACT
GOTO @check6
#ELSEACT
GOTO @next71
[@next71]
#IF
CHECK [507] 1
#ACT
GOTO @check7
#ELSEACT
GOTO @next81
[@next81]
#IF
CHECK [508] 1
#ACT
GOTO @check8
#ELSEACT
GOTO @next91
[@next91]
#IF
CHECK [509] 1
#ACT
GOTO @check9
#ELSEACT
GOTO @next101
[@next101]
#IF
CHECK [510] 1
#ACT
GOTO @check10
#ELSEACT
GOTO @next111
[@next111]
#IF
CHECK [511] 1
#ACT
GOTO @check11
#ELSEACT
GOTO @next121
[@next121]
#IF
CHECK [512] 1
#ACT
GOTO @check12
#ELSEACT
GOTO @next131
[@next131]
#IF
CHECK [513] 1
#ACT
GOTO @check13
#ELSEACT
GOTO @next141
[@next141]
#IF
CHECK [514] 1
#ACT
GOTO @check14
#ELSEACT
GOTO @next151
[@next151]
#IF
CHECK [515] 1
#ACT
GOTO @check15
#ELSEACT
GOTO @next161
[@next161]
#IF
CHECK [516] 1
#ACT
GOTO @check16
#ELSEACT
GOTO @next171
[@next171]
#IF
CHECK [517] 1
#ACT
GOTO @check17
#ELSEACT
GOTO @next181
[@next181]
#IF
CHECK [518] 1
#ACT
GOTO @check18
#ELSEACT
GOTO @next191
[@next191]
#IF
CHECK [519] 1
#ACT
GOTO @check19
#ELSEACT
GOTO @next201
[@next201]
#IF
CHECK [520] 1
#ACT
GOTO @check20
#ELSEACT
GOTO @starting
[@starting]
#SAY
受欢迎的。
从现在开始，许多怪物将出现在这个房间。
通过打败怪物来测试你的能力。
总共有20个阶段。
我想知道你是否能打败他们……
你会开始挑战吗?

<I'm ready./@start1>

[@oldman]
#ACT
MOVE 0 355 237

[@start1]
#ACT
SET [500] 0
SET [501] 1
Param1 EM001
Param2 13
Param3 16
MonGen 毒蜘蛛 1
MonGen 半兽人 2
MonGen 钉耙猫 2
[@check1]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say1
#ELSEACT
GOTO @say2
[@say1]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say2]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行/@start2>
<我受够了。让我走./@oldman>
[@start2]
#ACT
SET [501] 0
SET [502] 1
Param1 EM001
Param2 13
Param3 16
MonGen 洞蛆 1
MonGen 掷斧骷髅 1
MonGen 骷髅战将 1
MonGen 骷髅战士 3
[@check2]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say3
#ELSEACT
GOTO @say4
[@say3]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say4]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行/@start3>
<我受够了。让我走。/@oldman>
[@start3]
#ACT
SET [502] 0
SET [503] 1
Param1 EM001
Param2 13
Param3 16
MonGen 诅咒巫师 1
MonGen 诅咒僧侣 1
MonGen 萨满僵尸 1
MonGen 爬行僵尸 2
[@check3]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say5
#ELSEACT
GOTO @say6
[@say5]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say6]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start4>
<我受够了。让我走./@oldman>
[@start4]
#ACT
SET [503] 0
SET [504] 1
Param1 EM001
Param2 13
Param3 16
MonGen 沙虫 3
MonGen 半兽战士 2
MonGen 半兽勇士 1
[@check4]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say7
#ELSEACT
GOTO @say8
[@say7]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say8]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行/@start5>
<我受够了。让我走./@oldman>
[@start5]
#ACT
SET [504] 0
SET [505] 1
Param1 EM001
Param2 13
Param3 16
MonGen 粪虫 1
MonGen 黑暗战士 1
MonGen 火焰沃玛 2
MonGen 沃玛勇士 2
[@check5]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say9
#ELSEACT
GOTO @say10
[@say9]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say10]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start6>
<我受够了。让我走/@oldman>
[@start6]
#ACT
SET [505] 0
SET [506] 1
Param1 EM001
Param2 13
Param3 16
MonGen 巨型蠕虫 2
MonGen 蜈蚣 2
MonGen 钳虫 2
[@check6]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say11
#ELSEACT
GOTO @say12
[@say11]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say12]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start7>
<我受够了。让我走/@oldman>
[@start7]
#ACT
SET [506] 0
SET [507] 1
Param1 EM001
Param2 13
Param3 16
MonGen 楔蛾 2
MonGen 红野猪 1
MonGen 黑野猪 2
[@check7]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say13
#ELSEACT
GOTO @say14
[@say13]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say14]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start8>
<我受够了。让我走/@oldman>
[@start8]
#ACT
SET [507] 0
SET [508] 1
Param1 EM001
Param2 13
Param3 16
MonGen SpiderBat 1
MonGen GreatSpider 1
MonGen LureSpider 1
MonGen VenomSpider 2
[@check8]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say15
#ELSEACT
GOTO @say16
[@say15]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say16]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start9>
<我受够了。让我走/@oldman>
[@start9]
#ACT
SET [508] 0
SET [509] 1
Param1 EM001
Param2 13
Param3 16
MonGen BigApe 1
MonGen EvilApe 1
MonGen RedEvilApe 1
[@check9]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say17
#ELSEACT
GOTO @say18
[@say17]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say18]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start10>
<我受够了。让我走/@oldman>
[@start10]
#ACT
SET [509] 0
SET [510] 1
Param1 EM001
Param2 13
Param3 16
MonGen BoneArcher 1
MonGen BoneBlademan 1
MonGen BoneCaptain 1
[@check10]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say19
#ELSEACT
GOTO @say20
[@say19]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say20]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start11>
<我受够了。让我走/@oldman>
[@start11]
#ACT
SET [510] 0
SET [511] 1
Param1 EM001
Param2 13
Param3 16
MonGen LeftGuard 1
MonGen WindMinotaur 1
MonGen FireMinotaur 2
[@check11]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say21
#ELSEACT
GOTO @say22
[@say21]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say22]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start12>
<我受够了。让我走/@oldman>
[@start12]
#ACT
SET [511] 0
SET [512] 1
Param1 EM001
Param2 13
Param3 16
MonGen RightGuard 1
MonGen FireMinotaur 1
MonGen EvilApe 1
[@check12]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say23
#ELSEACT
GOTO @say24
[@say23]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say24]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start13>
<我受够了。让我走/@oldman>
[@start13]
#ACT
SET [512] 0
SET [513] 1
Param1 EM001
Param2 13
Param3 16
MonGen ZumaStatue 1
[@check13]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say25
#ELSEACT
GOTO @say26
[@say25]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say26]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start14>
<我受够了。让我走/@oldman>
[@start14]
#ACT
SET [513] 0
SET [514] 1
Param1 EM001
Param2 13
Param3 16
MonGen WedgeMoth 1
MonGen RightGuard 1
MonGen ZumaGuardian 1
[@check14]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say27
#ELSEACT
GOTO @say28
[@say27]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say28]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start15>
<我受够了。让我走/@oldman>
[@start15]
#ACT
SET [514] 0
SET [515] 1
Param1 EM001
Param2 13
Param3 16
MonGen AxeOma 1
[@check15]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say29
#ELSEACT
GOTO @say30
[@say29]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say30]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start16>
<我受够了。让我走/@oldman>
[@start16]
#ACT
SET [515] 0
SET [516] 1
Param1 EM001
Param2 13
Param3 16
MonGen OmaGuard 1
[@check16]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say31
#ELSEACT
GOTO @say32
[@say31]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say32]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start17>
<我受够了。让我走/@oldman>
[@start17]
#ACT
SET [516] 0
SET [517] 1
Param1 EM001
Param2 13
Param3 16
MonGen FlailOma 1
[@check17]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say33
#ELSEACT
GOTO @say34
[@say33]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say34]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start18>
<我受够了。让我走/@oldman>
[@start18]
#ACT
SET [517] 0
SET [518] 1
Param1 EM001
Param2 13
Param3 16
MonGen WoomaGuardian 1
[@check18]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say35
#ELSEACT
GOTO @say36
[@say35]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say36]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start19>
<我受够了。让我走/@oldman>
[@start19]
#ACT
SET [518] 0
SET [519] 1
Param1 EM001
Param2 13
Param3 16
MonGen Woomataurus 1
[@check19]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say37
#ELSEACT
GOTO @say38
[@say37]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say38]
#SAY
你已经把他们都打败了。
但还有更多的工作要做。
你会继续挑战吗?
<继续进行./@start20>
<我受够了。让我走/@oldman>
[@start20]
#ACT
SET [519] 0
SET [520] 1
Param1 EM001
Param2 13
Param3 16
MonGen EvilSnake 1
[@check20]
#IF
CHECKMON >= 1 em001
#ACT
GOTO @say39
#ELSEACT
GOTO @say40
[@say39]
#SAY
你还没有打败他们。
在吃掉怪物之前，
你不能进入下一阶段。
<Okay./@exit>
[@say40]
#SAY
Wow, you defeated them all!!
Oh... how can someone be so mighty...
I'm so honored to meet you.
You are the true hero can save the continent
when the crisis arrives.
I'll send you back to the village with a prize.
<Thank you./@finish>
[@finish]
#IF
CHECK [520] 1
#ACT
breaktimerecall
SET [500] 1
SET [501] 0
SET [502] 0
SET [503] 0
SET [504] 0
SET [505] 0
SET [506] 0
SET [507] 0
SET [508] 0
SET [509] 0
SET [510] 0
SET [511] 0
SET [512] 0
SET [513] 0
SET [514] 0
SET [515] 0
SET [516] 0
SET [517] 0
SET [518] 0
SET [519] 0
SET [520] 0
GIVEITEM BenedictionOil 1
GIVEGOLD 50000
MOVE 0 355 237
#ELSEACT
SET [500] 1
SET [501] 0
SET [502] 0
SET [503] 0
SET [504] 0
SET [505] 0
SET [506] 0
SET [507] 0
SET [508] 0
SET [509] 0
SET [510] 0
SET [511] 0
SET [512] 0
SET [513] 0
SET [514] 0
SET [515] 0
SET [516] 0
SET [517] 0
SET [518] 0
SET [519] 0
SET [520] 0
MOVE 0 355 237