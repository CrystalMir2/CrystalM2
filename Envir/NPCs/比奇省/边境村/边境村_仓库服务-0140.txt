[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
我不会帮助像你这样邪恶的人…
<存取/@Storage> 物品
<兑换/@mbind> 金融
<采购/@Buy>
您有 <$PARCELAMOUNT> 包裹等待收集。
<发送/@SendParcel> 包裹
<签收/@CollectParcel> 包裹
寻求 <信息/@Info> 

[@Storage]
#SAY
您想存什么，取什么?
 
<Back/@Main> - <Close/@exit>

[@Buy]
#SAY
你想买什么?
 
<Back/@Main> - <Close/@exit>

[@mbind]
#SAY
我可以用金币交换金条、金砖和金盒。
告诉我你的愿望。
 
交换: <金条/@GBar> 到 金币 - 佣金 2000金币
交换: <金砖/@GBBundle> 到 金币 - 佣金 10000金币
交换: <金盒/@GChest> 到 金币 - 佣金 20000金币
<Back/@main> - <Close/@exit>

[@GBar]
#IF
CHECKITEM 金条 1
#ACT
TAKEITEM 金砖 1
GIVEGOLD 998000
#ELSESAY
你没有金条给我兑换…
如果你有的话再来吧。
 
<Back/@mbind> - <Close/@exit>

[@GBBundle]
#IF
CHECKITEM 金砖 1
#ACT
TAKEITEM 金砖 1
GIVEGOLD 4990000
#ELSESAY
你没有金砖给我交换…
如果你有的话再来吧。
 
<Back/@mbind> - <Close/@exit>

[@GChest]
#IF
CHECKITEM 金盒 1
#ACT
TAKEITEM 金盒 1
GIVEGOLD 9980000
#ELSESAY
你没有金盒给我交换…
如果你有的话再来吧。
 
<Back/@mbind> - <Close/@exit>

[@Information]
#IF
CHECKQUEST 152 1
#ACT
SET [534] 1
#SAY
恐怕我对此一无所知。
有传言说酒馆里有个老疯子。
他还在研究一种古老的语言。
<Thank You/@exit>

[Types]
1

[Trade]
邮票
金条
金砖
金盒
