[@Main]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
欢迎，我能帮你多少?
 
<查看/@BuySell> 商品.
听听你的 <解释/@helpbooks> 技能介绍.
询问 <信息/@information>
 
<Close/@exit>

[@BuySell]
#SAY
请选择你想买或卖的书。
<回购/@BuyBack>
 
<Back/@Main>

[@helpbooks]
#SAY
解释你妈解释。没玩过传奇2吗？
 
Warrior:  <Skill List/@War1>
Wizard:   <Skill List/@Wiz1>
Taoist:   <Skill List/@Tao1>
Assassin: <Skill List/@Assa1>
Archer:   <Skill List/@Arc1>


[@War1]
#SAY
Level  7: Fencing
Level 15: Slaying
Level 22: Thrusting
Level 26: HalfMoon
Level 30: ShoulderDash
Level 32: TwinDragonBlade
Level 32: Entrapment
<More/@War2> <Back/@helpbooks>

[@War2]
#SAY
Level 35: FlamingSword
Level 36: LionRoar
Level 38: CrossHalfMoon
Level 38: BladeAvalanche
Level 39: ProtectionField
Level 44: Rage
Level 50: SlashingBurst
<Back/@helpbooks>

[@Wiz1]
#SAY
Level  7: FireBall
Level 12: Repulsion
Level 13: ElectricShock
Level 15: GreatFireball
Level 16: Hellfire
Level 17: Thunderbolt
<More/@Wiz2> <Back/@helpbooks>

[@Wiz2]
#SAY
Level 19: Teleport
Level 22: FireBang
Level 24: FireWall
Level 26: Lightning
Level 28: FrostCrunch
Level 30: Thunderstorm
Level 31: MagicShield
Level 32: TurnUndead
<More/@Wiz3> <Back/@helpBooks>

[@Wiz3]
#SAY
Level 33: Vampirism
Level 35: IceStrom
Level 38: FlameDisruptor
Level 41: Mirroring
Level 42: FlameField
Level 44: Blizzard
Level 49: MeteorStrike
Level 53: IceFreeze
<Back/@helpBooks>

[@Tao1]
#SAY
Level  7: Healing
Level  9: SpiritSword
Level 14: Poisoning
Level 18: SoulFireBall
Level 19: SummonSkeletonton
Level 20: Hiding
<More/@Tao2> <Back/@helpbooks>

[@Tao2]
#SAY
Level 21: MassHiding
Level 22: SoulShield
Level 23: Revelation
Level 25: BlessedArmour
Level 27: EnergyRepulsor
Level 28: TrapHexagon
Level 30: Purification
<More/@Tao3> <Back/@helpBooks>

[@Tao3]
#SAY
Level 31: MassHealing
Level 31: Hallucination
Level 33: UltimateEnhancer
Level 35: SummonShinsu
Level 37: Reincarnation
Level 38: SummonHolyDeva
Level 40: Curse
Level 43: PoisonCloud
Level 48: CelestalShield
<Back/@helpBooks>


[@Assa1]
#SAY
Level  7: FatalSword
Level 15: DoubleSlash
Level 20: Haste
Level 25: FlashDash
Level 27: LightBody
Level 30: HeavenlySword
Level 33: FireBurst
<More/@Assa2> <Back/@helpBooks>

[@Assa2]
#SAY
Level 33: Trap
Level 34: PoisonSword
Level 36: MoonLight
Level 38: MPEater
Level 40: SwiftFeet
Level 46: DarkBody
Level 50: MoonBlade
<Back/@helpBooks>


[@Arc1]
#SAY
Level  7: Focus
Level  9: StraightShot
Level 11: MentalState
Level 14: DoubleShot
Level 19: Meditation
Level 20: ElementalShot
Level 22: ExplosiveTrap
<More/@Arc2> <Back/@helpBooks>

[@Arc2]
#SAY
Level 23: Concentration
Level 26: VampireShot
Level 28: SummonVampire
Level 30: BackStep
Level 31: DelayedExplosion
Level 33: ElementalBarrier
Level 35: BindingShot
<More/@Arc3> <Back/@helpBooks>

[@Arc3]
#SAY
Level 37: SummonToad
Level 40: PoisonShot
Level 43: CrippleShot
Level 46: SummonSnakes
Level 48: NapalmShot
Level 50: OneWithNature
<Back/@helpBooks>

[@Information]
#IF
CHECKQUEST 152 1
#ACT
SET [533] 1
#SAY
嗯…我以前见过这些符号……
Mogu爵士在很多年前就问过这种语言。
去酒馆跟他谈谈。
<Thank You/@exit>


[Types]
20

[Trade]
基本剑术
攻杀剑术
火球术
抗拒火环
诱惑之光
大火球
地狱火
雷电术
瞬息移动
治愈术
精神力战法
施毒术
灵魂火符
召唤骷髅
绝命剑法
双刀术
必中
直击
精神状态
双重射击
冥想
