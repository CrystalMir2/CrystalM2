function QFunction$Login() {
    ctx.setCustomButton(0,0,0,"在线回收");
    ctx.setCustomButton(1,200,0,"便捷服务");
    ctx.sendMsg(6,`上次登陆时间:${ctx.load("LastLoginTime")}`)
    ctx.set("LastLoginTime",Time.Now);
    ctx.save("LastLoginTime",Time.Now);
    ctx.sendMsg(6,`本次登陆时间:${ctx.load("LastLoginTime")}`)
    var say = '';
    say += '<align=center><size=200%>欢迎来到 <color=#ff0000>Crystal Mir Of Legend II!</color></size></align>\n'
    say += '\n'
    say += '<margin=0.6em>CM2是一个用于制作MMORPG的游戏引擎,其特性如下:'
    say += '\n'
    say += '<margin=1em>1.服务端支持<color=#ffe104>Aot</color>编译,单文件跨平台<color=#ffe104>Window/Linux</color>部署'
    say += '\n'
    say += '2.客户端基于<color=#ffe104>Unity</color>.支持发布<color=#ffe104>Window/Linux/ios/Mac/Android</color>'
    say += '\n'
    say += '3.工具链一应俱全,<color=#ffe104>微端,资源,地图,数据库</color>编辑器'
    say += '\n'
    say += '4.更强更快的<color=#ffe104>JavaScirpt</color>脚本引擎,轻松上手编写多样化,趣味性的功能'
    say += '\n'
    say += '5.数据库支持<color=#ffe104>SQL</color>,兼容性,稳定性强 <sprite=3 color=#ffff55FF>'
    say += '\n'
    say +='<界面按钮/@QFunction$QF界面()>  <触发测试/@QFunction$QF界面()>  <常用命令/@QFunction$QF界面()>'
    say += `
    名称:<color=#ffe104>${padLeft(ctx.Name,20,' ')}</color>
    等级:<color=#ffe104>${padLeft(ctx.Level,20,' ')}</color> 
    背包:<color=#ffe104>${padLeft(ctx.BagSize,20,' ')}</color>
    性别:<color=#ffe104>${padLeft(ctx.Gender==0?'♀':'♂'.name,20,' ')}</color>
    IP  :<color=#ffe104>${padLeft(ctx.IP,20,' ')}</color>
        `
    //注意:由于特殊字符和中文占2个位置,字符串长度却是1,使用padStart对齐中英文时对不齐,
    return say;
}
function QFunction$UserItem() {
    return 双击触发$双击触发$Main(arguments[0],arguments[1],arguments[2],arguments[3]);
}

function QFunction$LevelUP() {
    var say = '恭喜 ,{'+ctx.Name+'#Red} \r\n';
    say += '升级到 ,{'+ctx.Level+'#Green} \r\n';
    say += '<传送/@QFunction$QF界面()>\r\n';
    return say;
}
function QFunction$Equipment() {
    return 穿戴触发$穿戴触发$Main(arguments[0],arguments[1],arguments[2],arguments[3]);
}
function QFunction$PickUp() {
    return 拾取触发$拾取触发$Main(arguments[0],arguments[1],arguments[2],arguments[3]);
}
function QFunction$Attack() {
    var say = 'Attack  \r\n';
    return say;
}
function QFunction$MagicBegin(magic) {

    //自身特效
    //string libraryName, int index,int imgCount, int interval,int replayCount,int delay
    ctx.playSelfEffect("Magic3",1880,10,100,0,0)//lib名必须正确.大小写敏感
    var say = `MagicBegin Spell:${magic.spell}`;
    ctx.sendMsg(6,say);
    return false;
    // return true;//返回ture.则会中断施法
}
function QFunction$MagicAttack(magic,target) {
    //目标命中特效
    //uint targetObjectID,string libraryName, int index,int imgCount, int interval,int replayCount,int delay
    if (target!=null)//判断是否命中目标
        ctx.playObjectEffect(target.ObjectID,"Monster/459",100,11,100,0,0)//lib名必须正确.大小写敏感

    var say = `MagicAttack:Spell:${magic.spell},target:${target.Info.Name},ObjectID:${target.ObjectID}`;
    ctx.sendMsg(6,say);
    return "";
}
function QFunction$KillMon(monster) {

    ctx.sendMsg(6,`杀怪触发:${monster.Info.Name}`);

    return  "";
}
function QFunction$Daily() {
    var say = '';
    return say;
}

function QFunction$QF界面() {
    var say = 'QF界面 ,{'+ctx.Name+'#Red} \r\n';
    say+= '<NPC功能总览/@QFunction$NPC功能总览()> ';
    return say;
}

function QFunction$NPC功能总览() {
    ctx.setGoods('屠龙','强效太阳水 8');
    var say = '';
    say+= '<寄售/@QFunction$Market()> ';
    say+= '<买卖/@QFunction$SellBuy()> ';
    say+= '<锻造/@QFunction$Refine()> ';
    say+= '<仓库/@QFunction$showStorage()> ';
    return say;
}

function QFunction$Market() {
    var say = '';
    ctx.showMarket();
    return say;
}
function QFunction$SellBuy() {
    var say = '';
    ctx.showSellBuy();
    return say;
}
function QFunction$Refine() {
    var say = '';
    ctx.showRefine();
    return say;
}
function QFunction$showStorage() {
    ctx.openClientDialog(0);
    return '';
}
function QFunction$OnClickBtnClick(btnID) {
    switch (btnID){
        case 0:
            ctx.openClientDialog(0);
            break
        default:
            ctx.openClientDialog(1);
            break
    }
    ctx.messageBox(`OnClickBtnClick`,`<确定/@QFunction$showStorage()>`)
    return '';
}


