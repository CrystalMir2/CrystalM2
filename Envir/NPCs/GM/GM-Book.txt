[@Main]
#SAY
Hello {<$USERNAME>/KHAKI}, my name is {<$NPCNAME>/KHAKI}.
 
<Buy/@Buy> Book
<Sell/@Sell> Book
 
Learn all <Warrior/@Warrior> , <Wizard/@Wizard> , <Taoist/@Taoist> , <Archer/@Archer> , <Assassin/@Assassin> spells.


[@Sell]
#SAY
Sell.
 
<Back/@Main>

[@Buy]
#SAY
Buy.
 
<Back/@Main>

[@Warrior]
#Act
GIVESKILL Fencing 3
GIVESKILL Slaying 3
GIVESKILL Thrusting 3
GIVESKILL HalfMoon 3
GIVESKILL ShoulderDash 3
GIVESKILL TwinDrakeBlade 3
GIVESKILL Entrapment 3
GIVESKILL FlamingSword 3
GIVESKILL LionRoar 3
GIVESKILL CrossHalfMoon 3
GIVESKILL BladeAvalanche 3
GIVESKILL ProtectionField 3
GIVESKILL Rage 3
GIVESKILL CounterAttack 3
GIVESKILL SlashingBurst 3
GIVESKILL Fury 3
[@Wizard]
#ACT
GIVESKILL FireBall 3
GIVESKILL Repulsion 3
GIVESKILL ElectricShock 3
GIVESKILL GreatFireBall 3
GIVESKILL HellFire 3
GIVESKILL ThunderBolt 3
GIVESKILL Teleport 3
GIVESKILL FireBang 3
GIVESKILL FireWall 3
GIVESKILL Lightning 3
GIVESKILL FrostCrunch 3
GIVESKILL ThunderStorm 3
GIVESKILL MagicShield 3
GIVESKILL TurnUndead 3
GIVESKILL Vampirism 3
GIVESKILL IceStorm 3
GIVESKILL FlameDisruptor 3
GIVESKILL Mirroring 3
GIVESKILL FlameField 3
GIVESKILL Blizzard 3
GIVESKILL MagicBooster 3
GIVESKILL MeteorStrike 3
GIVESKILL IceThrust 3
[@Taoist]
#ACT
GIVESKILL Healing 3
GIVESKILL SpiritSword 3
GIVESKILL Poisoning 3
GIVESKILL SoulFireBall 3
GIVESKILL SummonSkeleton 3
GIVESKILL Hiding 3
GIVESKILL MassHiding 3
GIVESKILL SoulShield 3
GIVESKILL Revelation 3
GIVESKILL BlessedArmour 3
GIVESKILL EnergyRepulsor 3
GIVESKILL TrapHexagon 3
GIVESKILL Purification 3
GIVESKILL MassHealing 3
GIVESKILL Hallucination 3
GIVESKILL UltimateEnhancer 3
GIVESKILL SummonShinsu 3
GIVESKILL Reincarnation 3
GIVESKILL SummonHolyDeva 3
GIVESKILL Curse 3
GIVESKILL Plague 3
GIVESKILL PoisonCloud 3
GIVESKILL EnergyShield 3
[@Assassin]
#ACT
GIVESKILL FatalSword 3
GIVESKILL DoubleSlash 3
GIVESKILL Haste 3
GIVESKILL FlashDash 3
GIVESKILL LightBody 3
GIVESKILL HeavenlySword 3
GIVESKILL FireBurst 3
GIVESKILL Trap 3
GIVESKILL PoisonSword 3
GIVESKILL MoonLight 3
GIVESKILL MPEater 3
GIVESKILL SwiftFeet 3
GIVESKILL DarkBody 3
GIVESKILL Hemorrhage 3
GIVESKILL CrescentSlash 3
[@Archer]
#ACT
GIVESKILL Focus 
GIVESKILL StraightShot 3
GIVESKILL DoubleShot 3
GIVESKILL ExplosiveTrap 3
GIVESKILL DelayedExplosion 3
GIVESKILL Meditation 3
GIVESKILL BackStep 3
GIVESKILL ElementalShot 3
GIVESKILL Concentration 3
GIVESKILL ElementalBarrier 3
GIVESKILL SummonVampire 3
GIVESKILL VampireShot 3
GIVESKILL SummonToad 3
GIVESKILL PoisonShot 3
GIVESKILL CrippleShot 3
GIVESKILL SummonSnakes 3
GIVESKILL NapalmShot 3
GIVESKILL OneWithNature 3

[Types]
20

[Trade]
基本剑术
攻杀剑术
刺杀剑术
半月弯刀
野蛮冲撞
双龙斩
捕绳剑
烈火剑法
狮子吼
圆月弯刀
攻破斩
护身气幕
剑气爆
反击
日闪
血龙剑法
NewWarrior2
火球术
抗拒火环
诱惑之光
大火球
地狱火
雷电术
瞬息移动
爆裂火焰
火墙
疾光电影
寒冰掌
地狱雷光
魔法盾
圣言术
嗜血术
冰咆哮
灭天火
分身术
火龙气焰
天霜冰环
深延术
流星火雨
冰焰术
NewWizard1
NewWizard2
治愈术
精神力战法
施毒术
灵魂火符
召唤骷髅
隐身术
集体隐身术
幽灵盾
心灵启示
神圣战甲术
气功波
困魔咒
净化术
群体治疗术
迷魂术
无极真气
召唤神兽
复活术
召唤月灵
诅咒术
瘟疫
毒云
阴阳盾
NewTaoist1
NewTaoist2
绝命剑法
双刀术
体迅风
拔刀术
风身术
迁移剑
烈风击
捕缚术
猛毒剑气
月影术
吸气
轻身步
烈火身
血风击
猫舌兰
NewAssassin1
NewAssassin2
必中
直击
双重射击
爆阱
欺诈
冥想
后闪
元素射击
专注
障碍
召唤
吸血射击
召唤蟾蜍
毒药射击
削弱射击
召唤蛇
汽油射击
融为一体
