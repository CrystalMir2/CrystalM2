[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
欢迎光临，我能为您做些什么?
 
<查看/@BuySell> 商品.
<修理/@Repair> 手镯或手套.
 
<Close/@exit>

[@BuySell]
#SAY
你想买或卖哪个项目?
<回购/@BuyBack>
 
<Back/@main>

[@BuyBack]
#SAY
这些都是还可以买回来的东西。
 
<Back/@main>

[@Repair]
#SAY
这些都是还可以买回来的东西。
  
<Back/@main>

[Types]
6

[Trade]
铁手镯
小手镯
皮制手套
银手镯
钢手镯
大手镯
