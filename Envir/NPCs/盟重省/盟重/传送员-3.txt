[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
你好，我是爱德温，流浪战士。
我能快速安全地把人和货物运送到其他地方。
只要付了钱，我就会护送你去任何地方。
你觉得怎么样?
 
我将使用这个 <服务/@tele>
<下次吧/@exit> 

[@tele]
#SAY
你想去哪个地方?
 
传送到: <比奇省/@move3> 2000 金币
传送到: <边境村/@move1> 2000 金币
传送到: <银杏村/@move6> 500 金币
传送到: <毒蛇山谷/@move2> 1000 金币
传送到: <道馆/@move4> 4000 金币
传送到: <沙城/@move5> 3000 金币
传送到: <下次吧/@exit>

[@move1]
#IF
CHECKGOLD > 2000
#ACT
MOVE n0 289 617
TAKEGOLD 2000
#ELSEACT
GOTO @B1

[@move6]
#IF
CHECKGOLD > 500
#ACT
MOVE n0 647 628
TAKEGOLD 500
#ELSEACT
GOTO @B1

[@move2]
#IF
CHECKGOLD > 1000
#ACT
MOVE 2 500 485
TAKEGOLD 1000
#ELSEACT
GOTO @B1

[@move3]
#IF
CHECKGOLD > 2000
#ACT
MOVE n0 296 221
TAKEGOLD 2000
#ELSEACT
GOTO @B1

[@move4]
#IF
CHECKGOLD > 4000
#ACT
MOVE 11 164 337
TAKEGOLD 4000
#ELSEACT
GOTO @B1

[@move5]
#IF
CHECKGOLD > 3000
#ACT
MOVE 4 264 257
TAKEGOLD 3000
#ELSEACT
GOTO @B1

[@B1]
#SAY
你没有足够的金币来使用我的服务!
走吧，别再浪费我的时间了!
 
<Close/@exit>

[Quests]
-102
