[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
你好,旅行者。我能为您做些什么?
 
<查看/@BuySell> 商品.
<修补/@Repair> 布料.
<交谈/@talk> 关于比奇城堡
 
<Close/@exit>

[@BuySell]
#SAY
你想买或卖哪个项目?
<回购/@BuyBack>
 
<Back/@main>

[@BuyBack]
#SAY
这些都是还可以买回来的东西。
 
<Back/@main>

[@Repair]
#SAY
您要修补布料吗?
 
<Back/@main>


[Types]
2
4
9
10

[Trade]
布衣(男)
布衣(女)
轻型盔甲(男)
轻型盔甲(女)
青铜头盔
魔法头盔
兽皮腰带
布鞋


[Quests]
-86
87
156

[@talk]
#IF
CHECK [540] 1
#ACT
GOTO @MAIN1-1
#ELSEACT
GOTO @CHECK2

[@CHECK2]
#IF
CHECK [541] 1
#ACT
GOTO @MAIN1-2
#ELSEACT
GOTO @MAIN1-3

[@MAIN1-1]
#SAY
你好旅行. .我一直听到关于皇帝的好事好事。
 
<Close/@exit>

[@MAIN1-2]
#SAY
你好再次旅行. .天气很好，不是吗?
 
<Close/@exit>

[@MAIN1-3]
#IF
CHECKQUEST 157 1
#SAY
谢谢你，旅行者，你为我节省了一些时间。
天哪，她不是说了一大堆废话吗?
 
So you want my help spreading the good deed's of the <Emperor/@Emperor>?
<Close/@exit>
#ELSESAY
How about you do something for me first? 
 
<Close@exit>

[@Emperor]
#ACT
SET [541] 1
#SAY
Very well traveler.. 
 
<Close/@exit>
