[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
欢迎光临!你想试试你的运气吗?
如果你输了，那别怪我!
你还想试试吗?然后把它捡起来。
如果钱的数量将超过限制，超过限制的钱将下降，所以小心!!
 
<买/@buy> 彩票.
<查看/@Bingo> 中奖情况!
<取消/@exit>

[@buy]
#SAY
Please pick up what you want to buy,
 
 
<Back/@main>

[@Bingo]
#SAY
  1等奖 : 1,000,000 金币
  2等奖 : 200,000 金币
  3等奖 : 100,000 金币
  4等奖 : 10,000 金币
  5等奖 : 1,000 金币
  6等奖 : 500 金币
 
<Back/@main>

[Type]

[Trade]
彩票


[Quests]
65
-65

[@talk]
#IF
CHECK [540] 1
#ACT
GOTO @MAIN1-1
#ELSEACT
GOTO @MAIN1-2

[@MAIN1-1]
#SAY
Traveler Traveler.. There's people going around ruining everything.
 
<Close/@exit>

[@MAIN1-2]
#SAY
Oh.. Sorry.. You startled me. Hello there..
"Can you be of any assistance?"
"I'm trying to spread a good word around about the <Emperor/@emperor>."
 
<Close/@exit>

[@emperor]
#ACT
SET [540] 1
#SAY
Oh sure <$USERNAME> but please visit me again soon.