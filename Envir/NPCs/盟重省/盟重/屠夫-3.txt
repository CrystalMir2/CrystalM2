[@MAIN]
#IF
CHECKPKPOINT > 2
#SAY
我不会帮助像你这样邪恶的人…
 
 
<Close/@exit>
#ELSEACT
GOTO @Main-1

[@Main-1]
#SAY
欢迎光临，我能为您做些什么?
 
<查看/@BuySell> 商品.
<询问/@Meathelp> 关于如何增肉.
 
<Close/@Exit>


[@BuySell]
#SAY
我将以高价购买高质量的产品。
但若肉沾了土，或被火焚烧
我将以低价买下它
<Buy Back/@BuyBack>
 
<Back/@Main>

[@BuyBack]
#SAY
项目的你仍然可以购买回来。
 
<Back/@Main>

[@Meathelp]
#SAY
可以从{母鸡/深红}，{鹿/深红}，{羊/深红}和{狼/深红}获得肉。
要从上述任何一种动物那里得到肉，首先要杀死动物。
一旦你杀死了它，按住ALT键，同时左键点击尸体几秒钟。
然后你应该看到一块肉出现在你的库存中。
使用魔法杀死动物，将其肉燃烧，使其质量降低到0。
 
<Back/@Main>

[Types]
15

[Trade]
鸡肉
肉

