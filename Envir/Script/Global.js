//脚本代码风格,变量大写开头,函数小写开头,驼峰式;常量,静态变量大写+下划线_分割
//脚本执行的上下文对象,常见有PlayerContext,MonsterContext,SystemContext

//函数,变量声明,功能介绍,同时可辅助代码编写提示
var ctx={
    Name: undefined,//昵称 string
    BagSize: undefined,//背包尺寸 num
    Level: undefined,//玩家或者怪物等级 num
    Gender: undefined,//玩家性别,0是男,1是女 num
    IP: undefined,//string,玩家IP
    sendMsg: (chatType, msgStr)=>{},//发送消息,0~4全体,5~7私人
    //uint targetObjectID,string libraryName(lib名必须正确.大小写敏感), int imageStart,int imgCount, int interval,int replayCount(-1=loop),int delay
    playObjectEffect:(targetObjectID,libraryName,imageStart,imgCount,interval,replayCount,delay)=>{},//目标播放特效
    playSelfEffect:(libraryName,imageStart,imgCount,interval,replayCount,delay)=>{},//自身播放特效

    //召唤宝宝,秒为单位
    remob:(monsterName,existTime)=>{},
    //给与物品到背包
    give:(itemName,itemCount)=>{},
    //从背包拿走物品
    take:(itemName,itemCount)=>{},

    giveGold:(count)=>{},//给与金币
    giveCredit:(count)=>{},//给与元宝
    givePearls:(count)=>{},//给与灵符
    giveSkill:(skillName,skillLevel)=>{},//给予技能,支持数据库技能名和id
    repairAll:()=>{},//特修全身装备
    //打开客户端窗口,0:背包,1:仓库,2:装备,3:技能
    openClientDialog:(type)=>{},
    //显示客户端的自定义按钮,按钮点击事件在QFunction中OnClickBtnClick接收<br>
    // 显示自定义按钮;
    // btnID 0~9:主界面,posX,posY:偏移坐标,默认横向排列
    // ;name:
    setCustomButton:(btnID=0,posX=0,posY=0,name="自定义按钮",bgLibName="GameScene",bgImageIndex=230)=>{},

    //保存全局数据,全服共用,持久化保存
    saveG:(key,value)=>{},
    //读取全局数据,全服共用,持久化保存
    loadG:(key,value)=>{},

    //保存玩家数据,仅本玩家可访问,持久化保存
    save:(key,value)=>{},
    //读取玩家数据,仅本玩家可访问,持久化保存
    load:(key,value)=>{},

    //读取临时数据,仅本玩家可访问,下线清空
    set:(key,value)=>{},
    //读取临时数据,仅本玩家可访问,下线清空
    get:(key,value)=>{},

    //读取今日数据,仅本玩家可访问,0点清空
    saveJ:(key,value)=>{},
    //读取今日数据,仅本玩家可访问,0点清空
    loadJ:(key,value)=>{},


}
//服务器名
ServerName = Server.Name;
//在线人数:
OnlinePlayer = Server.OnlinePlayer;

console.log("ServerName:"+ServerName);
console.log("OnlinePlayer:"+OnlinePlayer);

var Time = {
    Day: DateTime.Day,
    Month: DateTime.Month,
    Year: DateTime.Year,
    Hour: DateTime.Hour,
    Minute: DateTime.Minute,
    Second: DateTime.Second,
    //当前到Unix纪元的总毫秒数
    Minute: DateTime.NowMS,
    //当前到Unix纪元的总秒数
    NowS: DateTime.NowS,
    //当前格式化时间, 2023-09-15 12:12:12
    Now: DateTime.Now,
}

var args =  [];
args.clear = function(){
     args = [];
}

String.format = function() {
     if( arguments.length == 0 )
         return null;
     var str = arguments[0];
     for(var i=1;i<arguments.length;i++) {
         var re = new RegExp('\\{' + (i-1) + '\\}','gm');
         str = str.replace(re, arguments[i]);
     }
     return str;
 }

String.contains = function(src,sub) {
     return src.indexOf(sub) >= 0;
 }

String.charLen = function(s) {
    var totalLength = 0;
    var i;
    var charCode;
    for (i = 0; i < s.length; i++) {
        charCode = s.charCodeAt(i);
        if (charCode < 0x007f) {
            totalLength = totalLength + 1;
        } else if ((0x0080 <= charCode) && (charCode <= 0x07ff)) {
            totalLength += 2;
        } else if ((0x0800 <= charCode) && (charCode <= 0xffff)) {
            totalLength += 2;
        }
    }
    return totalLength;
}

function padLeft(src,len, ch) {
    if (ch == null) ch = ' ';
    if (!src.is(String))src = src.toString();
    var charLen = String.charLen(src);
    if (charLen >= len) return src;
    return Array(len - charLen + 1).join(ch) + src;
}

function padRight(src,len, ch) {
    if (ch == null) ch = ' ';
    if (!src.is(String))src = src.toString();
    var charLen = String.charLen(src);
    if (charLen >= len) return src;
    return src+Array(len - charLen + 1).join(ch) ;
}

Object.prototype.is= function (type) {
    if (type == undefined) {
        return false;
    }
    var _type = Object.prototype.toString.call(this);
    return String.contains(_type, type.name);

}

Number.prototype.is = Object.prototype.is;
Boolean.prototype.is = Object.prototype.is;
String.prototype.is = Object.prototype.is;

                   