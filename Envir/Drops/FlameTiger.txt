;Level1
;Gold
1/1 Gold 12500 LV1

;Potions
1/1 强效太阳水 LV1
1/1 强效太阳水 LV1
1/1 强效太阳水 LV1
1/2 强效太阳水 LV1
1/2 强效太阳水 LV1
1/2 强效太阳水 LV1
1/1 金疮药(大量) LV1 
1/1 魔法药(大量) LV1
1/1 金疮药(大量) LV1 
1/1 魔法药(大量) LV1 
1/1 金疮药(特大) LV1 
1/1 魔法药(特大) LV1 
1/1 金疮药(特大) LV1 
1/1 魔法药(特大) LV1
1/2 金疮药(大量) LV1 
1/2 魔法药(大量) LV1 
1/2 金疮药(大量) LV1 
1/2 魔法药(大量) LV1 
1/2 金疮药(特大) LV1 
1/2 魔法药(特大) LV1 
1/2 金疮药(特大) LV1 
1/2 魔法药(特大) LV1 
1/2 魔法药(大量) LV1 
1/2 魔法药(特大) LV1 
1/30 攻击药水(小) LV1 
1/30 魔法药水(小) LV1 
1/30 道力药水(小) LV1 
1/30 疾风药水(小) LV1 
1/30 体力药水(小) LV1 
1/30 魔力药水(小) LV1


;Armours
1/60 战神盔甲(男) LV1
1/60 战神盔甲(女) LV1
1/60 恶魔长袍(男) LV1
1/60 WizardRobe(F) LV1
1/60 幽灵战衣(男) LV1
1/60 钢化盔甲(男) LV1
1/60 钢化盔甲(女) LV1
1/60 幽灵战衣(女) LV1
1/60 炎红战衣(男) LV1
1/60 炎红战衣(女) LV1

;Rare Armours
1/8000 天衣无缝(男) LV1
1/8000 天衣无缝(女) LV1
1/8000 HeavenArcheyRobe(M) LV1
1/8000 HeavenArcheyRobe(F) LV1

;Weapons
1/250 裁决之杖  LV1
1/250 魔弓 LV1
1/250 骨玉权杖 LV1 
1/250 无极棍 LV1
1/250 暗黑刀 LV1
1/4000 修罗刀 LV1 
1/4000 屠龙 LV1 
1/4000 恶之弓 LV1
1/4000 噬魂法杖 LV1 
1/4000 龙纹剑  LV1


;Helmets
1/10 骷髅头盔 LV1
1/10 道士头盔 LV1 
1/50 黑铁头盔 LV1 
1/600 MajesticHelmet LV1

;Necklaces
1/90 虹魔项链 LV1 
1/90 魔血项链 LV1 
1/120 绿色项链 LV1 
1/120 恶魔铃铛 LV1 
1/120 灵魂项链 LV1 
1/110 幽灵项链 LV1 
1/110 天珠项链 LV1 
1/110 生命项链 LV1 
1/60 祈祷项链 LV1 
1/150 狂风项链 LV1 
1/500 MajesticNecklace LV1

;Bracelets
1/90 虹魔手镯 LV1 
1/90 魔血手镯 LV1 
1/105 骑士手镯 LV1 
1/105 心灵手镯 LV1 
1/105 龙之手镯 LV1 
1/500 MajesticWheel LV1

;Rings
1/90 虹魔戒指 LV1 
1/90 魔血戒指 LV1 
1/50 龙之戒指 LV1 
1/50 红宝石戒指 LV1 
1/50 铂金戒指 LV1 
1/100 祈祷戒指 LV1 
1/135 力量戒指 LV1 
1/135 紫碧螺 LV1 
1/135 泰坦戒指 LV1 
1/2 降妖除魔戒指 LV1 
1/2 珊瑚戒指 LV1 
1/500 MajesticRing LV1

;Rare Rings


;Boots
1/500 MajesticBoots LV1

;Belts
1/500 MajesticBelt LV1

;Stones
1/100 HolyHealthStone LV1 
1/100 HolyMagicStone LV1 
1/100 HolyPowerStone LV1 
1/300 HolyDCStone LV1 
1/300 HolyMCStone LV1 
1/300 HolySCStone LV1  

;Books
1/50 野蛮冲撞 LV1 
1/50 双龙斩 LV1 
1/50 捕绳剑 LV1 
1/50 烈火剑法 LV1
1/6000 护身气幕 LV1
1/6000 剑气爆 LV1
1/6000 反击 LV1
1/15000 血龙剑法 LV1
 
1/50 地狱雷光 LV1 
1/50 冰咆哮 LV1 
1/50 灭天火 LV1 
1/4000 分身术 LV1 
1/6000 火龙气焰 LV1
1/6000 深延术 LV1
1/6000 流星火雨 LV1
1/6000 冰焰术 LV1

1/50 烈风击 LV1
1/50 捕缚术 LV1
1/200 月影术 LV1
1/6000 血风击 LV1
1/6000 烈火身 LV1
1/15000 月华乱舞 LV1
 
1/50 净化术 LV1 
1/50 迷魂术 LV1 
1/200 复活术 LV1 
1/200 召唤月灵 LV1 
1/15000 诅咒术 LV1 
1/6000 毒云 LV1 
1/6000 瘟疫 LV1
1/6000 毒云 LV1
1/6000 阴阳盾 LV1
1/200 BladeStorm LV1
1/200 DragNet LV1
1/200 CrescentSlash LV1
1/200 疗愈圈 LV1
1/800 SliceNDice LV1

1/50 障碍 LV1
1/50 束缚射击 LV1
1/200 召唤蟾蜍 LV1
1/200 毒药射击 LV1
1/6000 削弱射击 LV1
1/6000 召唤蛇 LV1
1/100 汽油射击 LV1
1/15000 融为一体 LV1

;Gems
1/155 勇猛宝玉 LV1
1/155 魔性宝玉 LV1
1/155 仙界宝玉 LV1
1/155 疾风宝玉  LV1
1/160 集中宝玉 LV1
1/160 觉醒宝玉 LV1
1/200 疾风宝玉 LV1

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV1

;Level2
;Gold
1/1 Gold 12500 LV2

;Potions
1/1 强效太阳水 LV2
1/1 强效太阳水 LV2
1/1 强效太阳水 LV2
1/2 强效太阳水 LV2
1/2 强效太阳水 LV2
1/2 强效太阳水 LV2
1/1 金疮药(大量) LV2 
1/1 魔法药(大量) LV2
1/1 金疮药(大量) LV2 
1/1 魔法药(大量) LV2 
1/1 金疮药(特大) LV2 
1/1 魔法药(特大) LV2 
1/1 金疮药(特大) LV2 
1/1 魔法药(特大) LV2
1/2 金疮药(大量) LV2 
1/2 魔法药(大量) LV2 
1/2 金疮药(大量) LV2 
1/2 魔法药(大量) LV2 
1/2 金疮药(特大) LV2 
1/2 魔法药(特大) LV2 
1/2 金疮药(特大) LV2 
1/2 魔法药(特大) LV2 
1/2 魔法药(大量) LV2 
1/2 魔法药(特大) LV2 
1/30 攻击药水(小) LV2 
1/30 魔法药水(小) LV2 
1/30 道力药水(小) LV2 
1/30 疾风药水(小) LV2 
1/30 体力药水(小) LV2 
1/30 魔力药水(小) LV2


;Armours
1/60 战神盔甲(男) LV2
1/60 战神盔甲(女) LV2
1/60 恶魔长袍(男) LV2
1/60 WizardRobe(F) LV2
1/60 幽灵战衣(男) LV2
1/60 幽灵战衣(女) LV2
1/60 钢化盔甲(男) LV2
1/60 钢化盔甲(女) LV2
1/60 炎红战衣(男) LV2
1/60 炎红战衣(女) LV2

;Rare Armours
1/8000 天衣无缝(男) LV2
1/8000 天衣无缝(女) LV2
1/8000 HeavenArcheyRobe(M) LV2
1/8000 HeavenArcheyRobe(F) LV2

;Weapons
1/250 裁决之杖  LV2
1/250 魔弓 LV2
1/250 骨玉权杖 LV2 
1/250 无极棍 LV2
1/250 暗黑刀 LV2
1/4000 修罗刀 LV2 
1/4000 屠龙 LV2 
1/4000 噬魂法杖 LV2 
1/4000 恶之弓 LV2
1/4000 龙纹剑  LV2


;Helmets
1/10 骷髅头盔 LV2
1/10 道士头盔 LV2 
1/50 黑铁头盔 LV2 
1/600 MajesticHelmet LV2

;Necklaces
1/90 虹魔项链 LV2 
1/90 魔血项链 LV2 
1/120 绿色项链 LV2 
1/120 恶魔铃铛 LV2 
1/120 灵魂项链 LV2 
1/110 幽灵项链 LV2 
1/110 天珠项链 LV2 
1/110 生命项链 LV2 
1/60 祈祷项链 LV2 
1/150 狂风项链 LV2 
1/500 MajesticNecklace LV2

;Bracelets
1/90 虹魔手镯 LV2 
1/90 魔血手镯 LV2 
1/105 骑士手镯 LV2 
1/105 心灵手镯 LV2 
1/105 龙之手镯 LV2 
1/500 MajesticWheel LV2

;Rings
1/90 虹魔戒指 LV2 
1/90 魔血戒指 LV2 
1/50 龙之戒指 LV2 
1/50 红宝石戒指 LV2 
1/50 铂金戒指 LV2 
1/100 祈祷戒指 LV2 
1/135 力量戒指 LV2 
1/135 紫碧螺 LV2 
1/135 泰坦戒指 LV2 
1/2 降妖除魔戒指 LV2 
1/2 珊瑚戒指 LV2 
1/500 MajesticRing LV2

;Rare Rings


;Boots
1/500 MajesticBoots LV2

;Belts
1/500 MajesticBelt LV2

;Stones
1/100 HolyHealthStone LV2 
1/100 HolyMagicStone LV2 
1/100 HolyPowerStone LV2 
1/300 HolyDCStone LV2 
1/300 HolyMCStone LV2 
1/300 HolySCStone LV2  

;Books
1/50 野蛮冲撞 LV2 
1/50 双龙斩 LV2 
1/50 捕绳剑 LV2 
1/50 烈火剑法 LV2
1/6000 护身气幕 LV2
1/6000 剑气爆 LV2
1/6000 反击 LV2
1/15000 血龙剑法 LV2
 
1/50 地狱雷光 LV2 
1/50 冰咆哮 LV2 
1/50 灭天火 LV2 
1/4000 分身术 LV2 
1/6000 火龙气焰 LV2
1/6000 深延术 LV2
1/6000 流星火雨 LV2
1/6000 冰焰术 LV2

1/50 烈风击 LV2
1/50 捕缚术 LV2
1/200 月影术 LV2
1/6000 血风击 LV2
1/6000 烈火身 LV2
1/15000 月华乱舞 LV2
 
1/50 净化术 LV2 
1/50 迷魂术 LV2 
1/200 复活术 LV2 
1/200 召唤月灵 LV2 
1/15000 诅咒术 LV2 
1/6000 毒云 LV2 
1/6000 瘟疫 LV2
1/6000 毒云 LV2
1/6000 阴阳盾 LV2
1/200 BladeStorm LV2
1/200 DragNet LV2
1/200 CrescentSlash LV2
1/200 疗愈圈 LV2
1/800 SliceNDice LV2

1/50 障碍 LV2
1/50 束缚射击 LV2
1/200 召唤蟾蜍 LV2
1/200 毒药射击 LV2
1/6000 削弱射击 LV2
1/6000 召唤蛇 LV2
1/100 汽油射击 LV2
1/15000 融为一体 LV2

;Gems
1/155 勇猛宝玉 LV2
1/155 魔性宝玉 LV2
1/155 仙界宝玉 LV2
1/155 疾风宝玉  LV2
1/160 集中宝玉 LV2
1/160 觉醒宝玉 LV2
1/200 疾风宝玉 LV2

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV2

;Level3
;Gold
1/1 Gold 12500 LV3

;Potions
1/1 强效太阳水 LV3
1/1 强效太阳水 LV3
1/1 强效太阳水 LV3
1/2 强效太阳水 LV3
1/2 强效太阳水 LV3
1/2 强效太阳水 LV3
1/1 金疮药(大量) LV3 
1/1 魔法药(大量) LV3
1/1 金疮药(大量) LV3 
1/1 魔法药(大量) LV3 
1/1 金疮药(特大) LV3 
1/1 魔法药(特大) LV3 
1/1 金疮药(特大) LV3 
1/1 魔法药(特大) LV3
1/2 金疮药(大量) LV3 
1/2 魔法药(大量) LV3 
1/2 金疮药(大量) LV3 
1/2 魔法药(大量) LV3 
1/2 金疮药(特大) LV3 
1/2 魔法药(特大) LV3 
1/2 金疮药(特大) LV3 
1/2 魔法药(特大) LV3 
1/2 魔法药(大量) LV3 
1/2 魔法药(特大) LV3 
1/30 攻击药水(小) LV3 
1/30 魔法药水(小) LV3 
1/30 道力药水(小) LV3 
1/30 疾风药水(小) LV3 
1/30 体力药水(小) LV3 
1/30 魔力药水(小) LV3


;Armours
1/60 战神盔甲(男) LV3
1/60 战神盔甲(女) LV3
1/60 恶魔长袍(男) LV3
1/60 WizardRobe(F) LV3
1/60 幽灵战衣(男) LV3
1/60 钢化盔甲(男) LV3
1/60 钢化盔甲(女) LV3
1/60 幽灵战衣(女) LV3
1/60 炎红战衣(男) LV3
1/60 炎红战衣(女) LV3

;Rare Armours
1/8000 天衣无缝(男) LV3
1/8000 天衣无缝(女) LV3
1/8000 HeavenArcheyRobe(M) LV3
1/8000 HeavenArcheyRobe(F) LV3

;Weapons
1/250 裁决之杖  LV3
1/250 魔弓 LV3
1/250 骨玉权杖 LV3 
1/250 无极棍 LV3
1/250 暗黑刀 LV3
1/4000 修罗刀 LV3 
1/4000 屠龙 LV3 
1/4000 恶之弓 LV3
1/4000 噬魂法杖 LV3 
1/4000 龙纹剑  LV3


;Helmets
1/10 骷髅头盔 LV3
1/10 道士头盔 LV3 
1/50 黑铁头盔 LV3 
1/600 MajesticHelmet LV3

;Necklaces
1/90 虹魔项链 LV3 
1/90 魔血项链 LV3 
1/120 绿色项链 LV3 
1/120 恶魔铃铛 LV3 
1/120 灵魂项链 LV3 
1/110 幽灵项链 LV3 
1/110 天珠项链 LV3 
1/110 生命项链 LV3 
1/60 祈祷项链 LV3 
1/150 狂风项链 LV3 
1/500 MajesticNecklace LV3

;Bracelets
1/90 虹魔手镯 LV3 
1/90 魔血手镯 LV3 
1/105 骑士手镯 LV3 
1/105 心灵手镯 LV3 
1/105 龙之手镯 LV3 
1/500 MajesticWheel LV3

;Rings
1/90 虹魔戒指 LV3 
1/90 魔血戒指 LV3 
1/50 龙之戒指 LV3 
1/50 红宝石戒指 LV3 
1/50 铂金戒指 LV3 
1/100 祈祷戒指 LV3 
1/135 力量戒指 LV3 
1/135 紫碧螺 LV3 
1/135 泰坦戒指 LV3 
1/2 降妖除魔戒指 LV3 
1/2 珊瑚戒指 LV3 
1/500 MajesticRing LV3

;Rare Rings


;Boots
1/500 MajesticBoots LV3

;Belts
1/500 MajesticBelt LV3

;Stones
1/100 HolyHealthStone LV3 
1/100 HolyMagicStone LV3 
1/100 HolyPowerStone LV3 
1/300 HolyDCStone LV3 
1/300 HolyMCStone LV3 
1/300 HolySCStone LV3  

;Books
1/50 野蛮冲撞 LV3 
1/50 双龙斩 LV3 
1/50 捕绳剑 LV3 
1/50 烈火剑法 LV3
1/6000 护身气幕 LV3
1/6000 剑气爆 LV3
1/6000 反击 LV3
1/15000 血龙剑法 LV3
 
1/50 地狱雷光 LV3 
1/50 冰咆哮 LV3 
1/50 灭天火 LV3 
1/4000 分身术 LV3 
1/6000 火龙气焰 LV3
1/6000 深延术 LV3
1/6000 流星火雨 LV3
1/6000 冰焰术 LV3

1/50 烈风击 LV3
1/50 捕缚术 LV3
1/200 月影术 LV3
1/6000 血风击 LV3
1/6000 烈火身 LV3
1/15000 月华乱舞 LV3
 
1/50 净化术 LV3 
1/50 迷魂术 LV3 
1/200 复活术 LV3 
1/200 召唤月灵 LV3 
1/15000 诅咒术 LV3 
1/6000 毒云 LV3 
1/6000 瘟疫 LV3
1/6000 毒云 LV3
1/6000 阴阳盾 LV3
1/200 BladeStorm LV3
1/200 DragNet LV3
1/200 CrescentSlash LV3
1/200 疗愈圈 LV3
1/800 SliceNDice LV3

1/50 障碍 LV3
1/50 束缚射击 LV3
1/200 召唤蟾蜍 LV3
1/200 毒药射击 LV3
1/6000 削弱射击 LV3
1/6000 召唤蛇 LV3
1/100 汽油射击 LV3
1/15000 融为一体 LV3

;Gems
1/155 勇猛宝玉 LV3
1/155 魔性宝玉 LV3
1/155 仙界宝玉 LV3
1/155 疾风宝玉  LV3
1/160 集中宝玉 LV3
1/160 觉醒宝玉 LV3
1/200 疾风宝玉 LV3

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV3

;Level4
;Gold
1/1 Gold 12500 LV4

;Potions
1/1 强效太阳水 LV4
1/1 强效太阳水 LV4
1/1 强效太阳水 LV4
1/2 强效太阳水 LV4
1/2 强效太阳水 LV4
1/2 强效太阳水 LV4
1/1 金疮药(大量) LV4 
1/1 魔法药(大量) LV4
1/1 金疮药(大量) LV4 
1/1 魔法药(大量) LV4 
1/1 金疮药(特大) LV4 
1/1 魔法药(特大) LV4 
1/1 金疮药(特大) LV4 
1/1 魔法药(特大) LV4
1/2 金疮药(大量) LV4 
1/2 魔法药(大量) LV4 
1/2 金疮药(大量) LV4 
1/2 魔法药(大量) LV4 
1/2 金疮药(特大) LV4 
1/2 魔法药(特大) LV4 
1/2 金疮药(特大) LV4 
1/2 魔法药(特大) LV4 
1/2 魔法药(大量) LV4 
1/2 魔法药(特大) LV4 
1/30 攻击药水(小) LV4 
1/30 魔法药水(小) LV4 
1/30 道力药水(小) LV4 
1/30 疾风药水(小) LV4 
1/30 体力药水(小) LV4 
1/30 魔力药水(小) LV4


;Armours
1/60 战神盔甲(男) LV4
1/60 战神盔甲(女) LV4
1/60 恶魔长袍(男) LV4
1/60 WizardRobe(F) LV4
1/60 钢化盔甲(男) LV4
1/60 钢化盔甲(女) LV4
1/60 幽灵战衣(男) LV4
1/60 幽灵战衣(女) LV4
1/60 炎红战衣(男) LV4
1/60 炎红战衣(女) LV4

;Rare Armours
1/8000 天衣无缝(男) LV4
1/8000 天衣无缝(女) LV4
1/8000 HeavenArcheyRobe(M) LV4
1/8000 HeavenArcheyRobe(F) LV4

;Weapons
1/250 裁决之杖  LV4
1/250 骨玉权杖 LV4 
1/250 魔弓 LV4
1/250 无极棍 LV4
1/250 暗黑刀 LV4
1/4000 修罗刀 LV4 
1/4000 屠龙 LV4 
1/4000 恶之弓 LV4
1/4000 噬魂法杖 LV4 
1/4000 龙纹剑  LV4


;Helmets
1/10 骷髅头盔 LV4
1/10 道士头盔 LV4 
1/50 黑铁头盔 LV4 
1/600 MajesticHelmet LV4

;Necklaces
1/90 虹魔项链 LV4 
1/90 魔血项链 LV4 
1/120 绿色项链 LV4 
1/120 恶魔铃铛 LV4 
1/120 灵魂项链 LV4 
1/110 幽灵项链 LV4 
1/110 天珠项链 LV4 
1/110 生命项链 LV4 
1/60 祈祷项链 LV4 
1/150 狂风项链 LV4 
1/500 MajesticNecklace LV4

;Bracelets
1/90 虹魔手镯 LV4 
1/90 魔血手镯 LV4 
1/105 骑士手镯 LV4 
1/105 心灵手镯 LV4 
1/105 龙之手镯 LV4 
1/500 MajesticWheel LV4

;Rings
1/90 虹魔戒指 LV4 
1/90 魔血戒指 LV4 
1/50 龙之戒指 LV4 
1/50 红宝石戒指 LV4 
1/50 铂金戒指 LV4 
1/100 祈祷戒指 LV4 
1/135 力量戒指 LV4 
1/135 紫碧螺 LV4 
1/135 泰坦戒指 LV4 
1/2 降妖除魔戒指 LV4 
1/2 珊瑚戒指 LV4 
1/500 MajesticRing LV4

;Rare Rings


;Boots
1/500 MajesticBoots LV4

;Belts
1/500 MajesticBelt LV4

;Stones
1/100 HolyHealthStone LV4 
1/100 HolyMagicStone LV4 
1/100 HolyPowerStone LV4 
1/300 HolyDCStone LV4 
1/300 HolyMCStone LV4 
1/300 HolySCStone LV4  

;Books
1/50 野蛮冲撞 LV4 
1/50 双龙斩 LV4 
1/50 捕绳剑 LV4 
1/50 烈火剑法 LV4
1/6000 护身气幕 LV4
1/6000 剑气爆 LV4
1/6000 反击 LV4
1/15000 血龙剑法 LV4
 
1/50 地狱雷光 LV4 
1/50 冰咆哮 LV4 
1/50 灭天火 LV4 
1/4000 分身术 LV4 
1/6000 火龙气焰 LV4
1/6000 深延术 LV4
1/6000 流星火雨 LV4
1/6000 冰焰术 LV4

1/50 烈风击 LV4
1/50 捕缚术 LV4
1/200 月影术 LV4
1/6000 血风击 LV4
1/6000 烈火身 LV4
1/15000 月华乱舞 LV4
 
1/50 净化术 LV4 
1/50 迷魂术 LV4 
1/200 复活术 LV4 
1/200 召唤月灵 LV4 
1/15000 诅咒术 LV4 
1/6000 毒云 LV4 
1/6000 瘟疫 LV4
1/6000 毒云 LV4
1/6000 阴阳盾 LV4
1/200 BladeStorm LV4
1/200 DragNet LV4
1/200 CrescentSlash LV4
1/200 疗愈圈 LV4
1/800 SliceNDice LV4

1/50 障碍 LV4
1/50 束缚射击 LV4
1/200 召唤蟾蜍 LV4
1/200 毒药射击 LV4
1/6000 削弱射击 LV4
1/6000 召唤蛇 LV4
1/100 汽油射击 LV4
1/15000 融为一体 LV4

;Gems
1/155 勇猛宝玉 LV4
1/155 魔性宝玉 LV4
1/155 仙界宝玉 LV4
1/155 疾风宝玉  LV4
1/160 集中宝玉 LV4
1/160 觉醒宝玉 LV4
1/200 疾风宝玉 LV4

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV4

;Level5
;Gold
1/1 Gold 12500 LV5

;Potions
1/1 强效太阳水 LV5
1/1 强效太阳水 LV5
1/1 强效太阳水 LV5
1/2 强效太阳水 LV5
1/2 强效太阳水 LV5
1/2 强效太阳水 LV5
1/1 金疮药(大量) LV5 
1/1 魔法药(大量) LV5
1/1 金疮药(大量) LV5 
1/1 魔法药(大量) LV5 
1/1 金疮药(特大) LV5 
1/1 魔法药(特大) LV5 
1/1 金疮药(特大) LV5 
1/1 魔法药(特大) LV5
1/2 金疮药(大量) LV5 
1/2 魔法药(大量) LV5 
1/2 金疮药(大量) LV5 
1/2 魔法药(大量) LV5 
1/2 金疮药(特大) LV5 
1/2 魔法药(特大) LV5 
1/2 金疮药(特大) LV5 
1/2 魔法药(特大) LV5 
1/2 魔法药(大量) LV5 
1/2 魔法药(特大) LV5 
1/30 攻击药水(小) LV5 
1/30 魔法药水(小) LV5 
1/30 道力药水(小) LV5 
1/30 疾风药水(小) LV5 
1/30 体力药水(小) LV5 
1/30 魔力药水(小) LV5


;Armours
1/60 战神盔甲(男) LV5
1/60 战神盔甲(女) LV5
1/60 恶魔长袍(男) LV5
1/60 WizardRobe(F) LV5
1/60 钢化盔甲(男) LV5
1/60 钢化盔甲(女) LV5
1/60 幽灵战衣(男) LV5
1/60 幽灵战衣(女) LV5
1/60 炎红战衣(男) LV5
1/60 炎红战衣(女) LV5

;Rare Armours
1/8000 天衣无缝(男) LV5
1/8000 天衣无缝(女) LV5
1/8000 HeavenArcheyRobe(M) LV5
1/8000 HeavenArcheyRobe(F) LV5

;Weapons
1/250 裁决之杖  LV5
1/250 魔弓 LV5
1/250 骨玉权杖 LV5 
1/250 无极棍 LV5
1/250 暗黑刀 LV5
1/4000 修罗刀 LV5 
1/4000 屠龙 LV5 
1/4000 恶之弓 LV5
1/4000 噬魂法杖 LV5 
1/4000 龙纹剑  LV5


;Helmets
1/10 骷髅头盔 LV5
1/10 道士头盔 LV5 
1/50 黑铁头盔 LV5 
1/600 MajesticHelmet LV5

;Necklaces
1/90 虹魔项链 LV5 
1/90 魔血项链 LV5 
1/120 绿色项链 LV5 
1/120 恶魔铃铛 LV5 
1/120 灵魂项链 LV5 
1/110 幽灵项链 LV5 
1/110 天珠项链 LV5 
1/110 生命项链 LV5 
1/60 祈祷项链 LV5 
1/150 狂风项链 LV5 
1/500 MajesticNecklace LV5

;Bracelets
1/90 虹魔手镯 LV5 
1/90 魔血手镯 LV5 
1/105 骑士手镯 LV5 
1/105 心灵手镯 LV5 
1/105 龙之手镯 LV5 
1/500 MajesticWheel LV5

;Rings
1/90 虹魔戒指 LV5 
1/90 魔血戒指 LV5 
1/50 龙之戒指 LV5 
1/50 红宝石戒指 LV5 
1/50 铂金戒指 LV5 
1/100 祈祷戒指 LV5 
1/135 力量戒指 LV5 
1/135 紫碧螺 LV5 
1/135 泰坦戒指 LV5 
1/2 降妖除魔戒指 LV5 
1/2 珊瑚戒指 LV5 
1/500 MajesticRing LV5

;Rare Rings


;Boots
1/500 MajesticBoots LV5

;Belts
1/500 MajesticBelt LV5

;Stones
1/100 HolyHealthStone LV5 
1/100 HolyMagicStone LV5 
1/100 HolyPowerStone LV5 
1/300 HolyDCStone LV5 
1/300 HolyMCStone LV5 
1/300 HolySCStone LV5  

;Books
1/50 野蛮冲撞 LV5 
1/50 双龙斩 LV5 
1/50 捕绳剑 LV5 
1/50 烈火剑法 LV5
1/6000 护身气幕 LV5
1/6000 剑气爆 LV5
1/6000 反击 LV5
1/15000 血龙剑法 LV5
 
1/50 地狱雷光 LV5 
1/50 冰咆哮 LV5 
1/50 灭天火 LV5 
1/4000 分身术 LV5 
1/6000 火龙气焰 LV5
1/6000 深延术 LV5
1/6000 流星火雨 LV5
1/6000 冰焰术 LV5

1/50 烈风击 LV5
1/50 捕缚术 LV5
1/200 月影术 LV5
1/6000 血风击 LV5
1/6000 烈火身 LV5
1/15000 月华乱舞 LV5
 
1/50 净化术 LV5 
1/50 迷魂术 LV5 
1/200 复活术 LV5 
1/200 召唤月灵 LV5 
1/15000 诅咒术 LV5 
1/6000 毒云 LV5 
1/6000 瘟疫 LV5
1/6000 毒云 LV5
1/6000 阴阳盾 LV5
1/200 BladeStorm LV5
1/200 DragNet LV5
1/200 CrescentSlash LV5
1/200 疗愈圈 LV5
1/800 SliceNDice LV5

1/50 障碍 LV5
1/50 束缚射击 LV5
1/200 召唤蟾蜍 LV5
1/200 毒药射击 LV5
1/6000 削弱射击 LV5
1/6000 召唤蛇 LV5
1/100 汽油射击 LV5
1/15000 融为一体 LV5

;Gems
1/155 勇猛宝玉 LV5
1/155 魔性宝玉 LV5
1/155 仙界宝玉 LV5
1/155 疾风宝玉  LV5
1/160 集中宝玉 LV5
1/160 觉醒宝玉 LV5
1/200 疾风宝玉 LV5

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV5

;Level6
;Gold
1/1 Gold 12500 LV6

;Potions
1/1 强效太阳水 LV6
1/1 强效太阳水 LV6
1/1 强效太阳水 LV6
1/2 强效太阳水 LV6
1/2 强效太阳水 LV6
1/2 强效太阳水 LV6
1/1 金疮药(大量) LV6 
1/1 魔法药(大量) LV6
1/1 金疮药(大量) LV6 
1/1 魔法药(大量) LV6 
1/1 金疮药(特大) LV6 
1/1 魔法药(特大) LV6 
1/1 金疮药(特大) LV6 
1/1 魔法药(特大) LV6
1/2 金疮药(大量) LV6 
1/2 魔法药(大量) LV6 
1/2 金疮药(大量) LV6 
1/2 魔法药(大量) LV6 
1/2 金疮药(特大) LV6 
1/2 魔法药(特大) LV6 
1/2 金疮药(特大) LV6 
1/2 魔法药(特大) LV6 
1/2 魔法药(大量) LV6 
1/2 魔法药(特大) LV6 
1/30 攻击药水(小) LV6 
1/30 魔法药水(小) LV6 
1/30 道力药水(小) LV6 
1/30 疾风药水(小) LV6 
1/30 体力药水(小) LV6 
1/30 魔力药水(小) LV6


;Armours
1/60 战神盔甲(男) LV6
1/60 战神盔甲(女) LV6
1/60 钢化盔甲(男) LV6
1/60 钢化盔甲(女) LV6
1/60 恶魔长袍(男) LV6
1/60 WizardRobe(F) LV6
1/60 幽灵战衣(男) LV6
1/60 幽灵战衣(女) LV6
1/60 炎红战衣(男) LV6
1/60 炎红战衣(女) LV6

;Rare Armours
1/8000 天衣无缝(男) LV6
1/8000 天衣无缝(女) LV6
1/8000 HeavenArcheyRobe(M) LV6
1/8000 HeavenArcheyRobe(F) LV6

;Weapons
1/250 裁决之杖  LV6
1/250 骨玉权杖 LV6 
1/250 魔弓 LV6
1/250 无极棍 LV6
1/250 暗黑刀 LV6
1/4000 修罗刀 LV6 
1/4000 屠龙 LV6 
1/4000 噬魂法杖 LV6 
1/4000 恶之弓 LV6
1/4000 龙纹剑  LV6


;Helmets
1/10 骷髅头盔 LV6
1/10 道士头盔 LV6 
1/50 黑铁头盔 LV6 
1/600 MajesticHelmet LV6

;Necklaces
1/90 虹魔项链 LV6 
1/90 魔血项链 LV6 
1/120 绿色项链 LV6 
1/120 恶魔铃铛 LV6 
1/120 灵魂项链 LV6 
1/110 幽灵项链 LV6 
1/110 天珠项链 LV6 
1/110 生命项链 LV6 
1/60 祈祷项链 LV6 
1/150 狂风项链 LV6 
1/500 MajesticNecklace LV6

;Bracelets
1/90 虹魔手镯 LV6 
1/90 魔血手镯 LV6 
1/105 骑士手镯 LV6 
1/105 心灵手镯 LV6 
1/105 龙之手镯 LV6 
1/500 MajesticWheel LV6

;Rings
1/90 虹魔戒指 LV6 
1/90 魔血戒指 LV6 
1/50 龙之戒指 LV6 
1/50 红宝石戒指 LV6 
1/50 铂金戒指 LV6 
1/100 祈祷戒指 LV6 
1/135 力量戒指 LV6 
1/135 紫碧螺 LV6 
1/135 泰坦戒指 LV6 
1/2 降妖除魔戒指 LV6 
1/2 珊瑚戒指 LV6 
1/500 MajesticRing LV6

;Rare Rings


;Boots
1/500 MajesticBoots LV6

;Belts
1/500 MajesticBelt LV6

;Stones
1/100 HolyHealthStone LV6 
1/100 HolyMagicStone LV6 
1/100 HolyPowerStone LV6 
1/300 HolyDCStone LV6 
1/300 HolyMCStone LV6 
1/300 HolySCStone LV6  

;Books
1/50 野蛮冲撞 LV6 
1/50 双龙斩 LV6 
1/50 捕绳剑 LV6 
1/50 烈火剑法 LV6
1/6000 护身气幕 LV6
1/6000 剑气爆 LV6
1/6000 反击 LV6
1/15000 血龙剑法 LV6
 
1/50 地狱雷光 LV6 
1/50 冰咆哮 LV6 
1/50 灭天火 LV6 
1/4000 分身术 LV6 
1/6000 火龙气焰 LV6
1/6000 深延术 LV6
1/6000 流星火雨 LV6
1/6000 冰焰术 LV6

1/50 烈风击 LV6
1/50 捕缚术 LV6
1/200 月影术 LV6
1/6000 血风击 LV6
1/6000 烈火身 LV6
1/15000 月华乱舞 LV6
 
1/50 净化术 LV6 
1/50 迷魂术 LV6 
1/200 复活术 LV6 
1/200 召唤月灵 LV6 
1/15000 诅咒术 LV6 
1/6000 毒云 LV6 
1/6000 瘟疫 LV6
1/6000 毒云 LV6
1/6000 阴阳盾 LV6
1/200 BladeStorm LV6
1/200 DragNet LV6
1/200 CrescentSlash LV6
1/200 疗愈圈 LV6
1/800 SliceNDice LV6

1/50 障碍 LV6
1/50 束缚射击 LV6
1/200 召唤蟾蜍 LV6
1/200 毒药射击 LV6
1/6000 削弱射击 LV6
1/6000 召唤蛇 LV6
1/100 汽油射击 LV6
1/15000 融为一体 LV6

;Gems
1/155 勇猛宝玉 LV6
1/155 魔性宝玉 LV6
1/155 仙界宝玉 LV6
1/155 疾风宝玉  LV6
1/160 集中宝玉 LV6
1/160 觉醒宝玉 LV6
1/200 疾风宝玉 LV6

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV6

;Level7
;Gold
1/1 Gold 12500 LV7

;Potions
1/1 强效太阳水 LV7
1/1 强效太阳水 LV7
1/1 强效太阳水 LV7
1/2 强效太阳水 LV7
1/2 强效太阳水 LV7
1/2 强效太阳水 LV7
1/1 金疮药(大量) LV7 
1/1 魔法药(大量) LV7
1/1 金疮药(大量) LV7 
1/1 魔法药(大量) LV7 
1/1 金疮药(特大) LV7 
1/1 魔法药(特大) LV7 
1/1 金疮药(特大) LV7 
1/1 魔法药(特大) LV7
1/2 金疮药(大量) LV7 
1/2 魔法药(大量) LV7 
1/2 金疮药(大量) LV7 
1/2 魔法药(大量) LV7 
1/2 金疮药(特大) LV7 
1/2 魔法药(特大) LV7 
1/2 金疮药(特大) LV7 
1/2 魔法药(特大) LV7 
1/2 魔法药(大量) LV7 
1/2 魔法药(特大) LV7 
1/30 攻击药水(小) LV7 
1/30 魔法药水(小) LV7 
1/30 道力药水(小) LV7 
1/30 疾风药水(小) LV7 
1/30 体力药水(小) LV7 
1/30 魔力药水(小) LV7


;Armours
1/60 战神盔甲(男) LV7
1/60 战神盔甲(女) LV7
1/60 钢化盔甲(男) LV7
1/60 钢化盔甲(女) LV7
1/60 恶魔长袍(男) LV7
1/60 WizardRobe(F) LV7
1/60 幽灵战衣(男) LV7
1/60 幽灵战衣(女) LV7
1/60 炎红战衣(男) LV7
1/60 炎红战衣(女) LV7

;Rare Armours
1/8000 天衣无缝(男) LV7
1/8000 天衣无缝(女) LV7
1/8000 HeavenArcheyRobe(M) LV7
1/8000 HeavenArcheyRobe(F) LV7

;Weapons
1/250 裁决之杖  LV7
1/250 骨玉权杖 LV7 
1/250 魔弓 LV7
1/250 无极棍 LV7
1/250 暗黑刀 LV7
1/4000 修罗刀 LV7 
1/4000 屠龙 LV7 
1/4000 噬魂法杖 LV7 
1/4000 恶之弓 LV7
1/4000 龙纹剑  LV7


;Helmets
1/10 骷髅头盔 LV7
1/10 道士头盔 LV7 
1/50 黑铁头盔 LV7 
1/600 MajesticHelmet LV7

;Necklaces
1/90 虹魔项链 LV7 
1/90 魔血项链 LV7 
1/120 绿色项链 LV7 
1/120 恶魔铃铛 LV7 
1/120 灵魂项链 LV7 
1/110 幽灵项链 LV7 
1/110 天珠项链 LV7 
1/110 生命项链 LV7 
1/60 祈祷项链 LV7 
1/150 狂风项链 LV7 
1/500 MajesticNecklace LV7

;Bracelets
1/90 虹魔手镯 LV7 
1/90 魔血手镯 LV7 
1/105 骑士手镯 LV7 
1/105 心灵手镯 LV7 
1/105 龙之手镯 LV7 
1/500 MajesticWheel LV7

;Rings
1/90 虹魔戒指 LV7 
1/90 魔血戒指 LV7 
1/50 龙之戒指 LV7 
1/50 红宝石戒指 LV7 
1/50 铂金戒指 LV7 
1/100 祈祷戒指 LV7 
1/135 力量戒指 LV7 
1/135 紫碧螺 LV7 
1/135 泰坦戒指 LV7 
1/2 降妖除魔戒指 LV7 
1/2 珊瑚戒指 LV7 
1/500 MajesticRing LV7

;Rare Rings


;Boots
1/500 MajesticBoots LV7

;Belts
1/500 MajesticBelt LV7

;Stones
1/100 HolyHealthStone LV7 
1/100 HolyMagicStone LV7 
1/100 HolyPowerStone LV7 
1/300 HolyDCStone LV7 
1/300 HolyMCStone LV7 
1/300 HolySCStone LV7  

;Books
1/50 野蛮冲撞 LV7 
1/50 双龙斩 LV7 
1/50 捕绳剑 LV7 
1/50 烈火剑法 LV7
1/6000 护身气幕 LV7
1/6000 剑气爆 LV7
1/6000 反击 LV7
1/15000 血龙剑法 LV7
 
1/50 地狱雷光 LV7 
1/50 冰咆哮 LV7 
1/50 灭天火 LV7 
1/4000 分身术 LV7 
1/6000 火龙气焰 LV7
1/6000 深延术 LV7
1/6000 流星火雨 LV7
1/6000 冰焰术 LV7

1/50 烈风击 LV7
1/50 捕缚术 LV7
1/200 月影术 LV7
1/6000 血风击 LV7
1/6000 烈火身 LV7
1/15000 月华乱舞 LV7
 
1/50 净化术 LV7 
1/50 迷魂术 LV7 
1/200 复活术 LV7 
1/200 召唤月灵 LV7 
1/15000 诅咒术 LV7 
1/6000 毒云 LV7 
1/6000 瘟疫 LV7
1/6000 毒云 LV7
1/6000 阴阳盾 LV7
1/200 BladeStorm LV7
1/200 DragNet LV7
1/200 CrescentSlash LV7
1/200 疗愈圈 LV7
1/800 SliceNDice LV7

1/50 障碍 LV7
1/50 束缚射击 LV7
1/200 召唤蟾蜍 LV7
1/200 毒药射击 LV7
1/6000 削弱射击 LV7
1/6000 召唤蛇 LV7
1/100 汽油射击 LV7
1/15000 融为一体 LV7

;Gems
1/155 勇猛宝玉 LV7
1/155 魔性宝玉 LV7
1/155 仙界宝玉 LV7
1/155 疾风宝玉  LV7
1/160 集中宝玉 LV7
1/160 觉醒宝玉 LV7
1/200 疾风宝玉 LV7

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV7

;Level8
;Gold
1/1 Gold 12500 LV8

;Potions
1/1 强效太阳水 LV8
1/1 强效太阳水 LV8
1/1 强效太阳水 LV8
1/2 强效太阳水 LV8
1/2 强效太阳水 LV8
1/2 强效太阳水 LV8
1/1 金疮药(大量) LV8 
1/1 魔法药(大量) LV8
1/1 金疮药(大量) LV8 
1/1 魔法药(大量) LV8 
1/1 金疮药(特大) LV8 
1/1 魔法药(特大) LV8 
1/1 金疮药(特大) LV8 
1/1 魔法药(特大) LV8
1/2 金疮药(大量) LV8 
1/2 魔法药(大量) LV8 
1/2 金疮药(大量) LV8 
1/2 魔法药(大量) LV8 
1/2 金疮药(特大) LV8 
1/2 魔法药(特大) LV8 
1/2 金疮药(特大) LV8 
1/2 魔法药(特大) LV8 
1/2 魔法药(大量) LV8 
1/2 魔法药(特大) LV8 
1/30 攻击药水(小) LV8 
1/30 魔法药水(小) LV8 
1/30 道力药水(小) LV8 
1/30 疾风药水(小) LV8 
1/30 体力药水(小) LV8 
1/30 魔力药水(小) LV8


;Armours
1/60 战神盔甲(男) LV8
1/60 战神盔甲(女) LV8
1/60 钢化盔甲(男) LV8
1/60 钢化盔甲(女) LV8
1/60 恶魔长袍(男) LV8
1/60 WizardRobe(F) LV8
1/60 幽灵战衣(男) LV8
1/60 幽灵战衣(女) LV8
1/60 炎红战衣(男) LV8
1/60 炎红战衣(女) LV8

;Rare Armours
1/8000 天衣无缝(男) LV8
1/8000 天衣无缝(女) LV8
1/8000 HeavenArcheyRobe(M) LV8
1/8000 HeavenArcheyRobe(F) LV8

;Weapons
1/250 裁决之杖  LV8
1/250 骨玉权杖 LV8 
1/250 魔弓 LV8
1/250 无极棍 LV8
1/250 暗黑刀 LV8
1/4000 修罗刀 LV8 
1/4000 屠龙 LV8 
1/4000 恶之弓 LV8
1/4000 噬魂法杖 LV8 
1/4000 龙纹剑  LV8


;Helmets
1/10 骷髅头盔 LV8
1/10 道士头盔 LV8 
1/50 黑铁头盔 LV8 
1/600 MajesticHelmet LV8

;Necklaces
1/90 虹魔项链 LV8 
1/90 魔血项链 LV8 
1/120 绿色项链 LV8 
1/120 恶魔铃铛 LV8 
1/120 灵魂项链 LV8 
1/110 幽灵项链 LV8 
1/110 天珠项链 LV8 
1/110 生命项链 LV8 
1/60 祈祷项链 LV8 
1/150 狂风项链 LV8 
1/500 MajesticNecklace LV8

;Bracelets
1/90 虹魔手镯 LV8 
1/90 魔血手镯 LV8 
1/105 骑士手镯 LV8 
1/105 心灵手镯 LV8 
1/105 龙之手镯 LV8 
1/500 MajesticWheel LV8

;Rings
1/90 虹魔戒指 LV8 
1/90 魔血戒指 LV8 
1/50 龙之戒指 LV8 
1/50 红宝石戒指 LV8 
1/50 铂金戒指 LV8 
1/100 祈祷戒指 LV8 
1/135 力量戒指 LV8 
1/135 紫碧螺 LV8 
1/135 泰坦戒指 LV8 
1/2 降妖除魔戒指 LV8 
1/2 珊瑚戒指 LV8 
1/500 MajesticRing LV8

;Rare Rings


;Boots
1/500 MajesticBoots LV8

;Belts
1/500 MajesticBelt LV8

;Stones
1/100 HolyHealthStone LV8 
1/100 HolyMagicStone LV8 
1/100 HolyPowerStone LV8 
1/300 HolyDCStone LV8 
1/300 HolyMCStone LV8 
1/300 HolySCStone LV8  

;Books
1/50 野蛮冲撞 LV8 
1/50 双龙斩 LV8 
1/50 捕绳剑 LV8 
1/50 烈火剑法 LV8
1/6000 护身气幕 LV8
1/6000 剑气爆 LV8
1/6000 反击 LV8
1/15000 血龙剑法 LV8
 
1/50 地狱雷光 LV8 
1/50 冰咆哮 LV8 
1/50 灭天火 LV8 
1/4000 分身术 LV8 
1/6000 火龙气焰 LV8
1/6000 深延术 LV8
1/6000 流星火雨 LV8
1/6000 冰焰术 LV8

1/50 烈风击 LV8
1/50 捕缚术 LV8
1/200 月影术 LV8
1/6000 血风击 LV8
1/6000 烈火身 LV8
1/15000 月华乱舞 LV8
 
1/50 净化术 LV8 
1/50 迷魂术 LV8 
1/200 复活术 LV8 
1/200 召唤月灵 LV8 
1/15000 诅咒术 LV8 
1/6000 毒云 LV8 
1/6000 瘟疫 LV8
1/6000 毒云 LV8
1/6000 阴阳盾 LV8
1/200 BladeStorm LV8
1/200 DragNet LV8
1/200 CrescentSlash LV8
1/200 疗愈圈 LV8
1/800 SliceNDice LV8

1/50 障碍 LV8
1/50 束缚射击 LV8
1/200 召唤蟾蜍 LV8
1/200 毒药射击 LV8
1/6000 削弱射击 LV8
1/6000 召唤蛇 LV8
1/100 汽油射击 LV8
1/15000 融为一体 LV8

;Gems
1/155 勇猛宝玉 LV8
1/155 魔性宝玉 LV8
1/155 仙界宝玉 LV8
1/155 疾风宝玉  LV8
1/160 集中宝玉 LV8
1/160 觉醒宝玉 LV8
1/200 疾风宝玉 LV8

;Awakening Souls

1/1 觉醒之魂0 LV

1/20 觉醒之魂0 LV8

