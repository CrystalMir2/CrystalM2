;Level 1
;Gold
1/1 Gold 25000 LV1

;Pots + Other
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 太阳水 LV1
1/1 祝福油 LV1
1/10 GoldenBenedictionOil LV1


1/10 觉醒之魂0 LV1
1/20 觉醒之魂0 LV1

1/150 OrkMohzatRune LV1

;Weapons
1/250 开天 LV1
1/250 力量之弓 LV1
1/250 镇天 LV1
1/250 玄天 LV1
1/650 月轮剑 LV1
1/650 恐怖弓 LV1
1/650 月轮枪 LV1
1/650 变种剑 LV1
1/750 黑虎斧 LV1
1/750 致命弓 LV1
1/750 火莲杖 LV1
1/750 鹤羽扇 LV1
1/1150 碧血混水剑 LV1
1/1150 虹玄混地剑 LV1
1/1150 翊仙混真扇 LV1
1/1150 阿普斯弓 LV1

;Armour
1/300 黑虎甲胄(男) LV1
1/300 黑虎甲胄(女) LV1
1/300 莲盛魔衣(男) LV1
1/300 莲盛魔衣(女) LV1
1/300 鹤羽圣衣(男) LV1
1/300 鹤羽圣衣(女) LV1
1/450 赤之魔甲(男) LV1
1/450 赤之魔甲(女) LV1
1/450 青之魔甲(男) LV1
1/450 青之魔甲(女) LV1
1/450 绿之魔甲(男) LV1
1/450 绿之魔甲(女) LV1
1/750 WatcherSuit(M) LV1
1/750 WatcherSuit(F) LV1
1/750 鳞甲(男) LV1
1/750 鳞甲(女) LV1

;Upgrade Armour
1/4500 天衣无缝(男) LV1
1/4500 天衣无缝(女) LV1
1/4500 HeavenRedArmour(M) LV1
1/4500 HeavenRedArmour(F) LV1
1/4500 HeavenGrnArmour(M) LV1
1/4500 HeavenGrnArmour(F) LV1

;Helmets
1/40 英雄头盔 LV1
1/40 贤者帽 LV1
1/40 通天冠 LV1
1/40 修罗头盔 LV1
1/250 贝玉头盔 LV1
1/260 PuiYuHelmet LV1
1/570 WatcherHelmet LV1

;Necklace
1/50 破角 LV1
1/50 针坠 LV1
1/50 心如明镜 LV1
1/275 曜紫天坠 LV1
1/275 秀女十字盘 LV1
1/275 金刚陶坠 LV1
1/500 DefenderNecklace LV1

;Bracelets
1/50 铁魔双轮 LV1
1/50 玉女天使 LV1
1/50 曜火虹轮 LV1
1/270 暴虎归山 LV1
1/275 曜宝探魔轮 LV1
1/275 五行悟思轮 LV1
1/575 WatcherGloves LV1
1/500 DefenderGlove LV1

;Rings
1/50 盟约戒指 LV1
1/50 五行连环 LV1
1/50 血玉环 LV1
1/275 飞曜环 LV1
1/275 金刚伏魔环 LV1
1/275 金龙戒指 LV1
1/500 DefenderRing LV1

;Shoes
1/550 WatcherBoots LV1

;Belts


;Medals


;Shields
1/700 OrcsShield LV1

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV1
1/10 魔性宝玉  LV1
1/10 仙界宝玉 LV1
1/15 守护宝玉 LV1
1/15 制魔宝玉 LV1
1/15 强化宝玉 LV1

1/30 中毒宝玉 LV1
1/32 酷寒宝玉 LV1
1/35 集中宝玉 LV1
1/35 疾风宝玉 LV1
1/40 忍耐宝玉 LV1

1/20 勇猛神珠 LV1
1/20 魔性神珠 LV1
1/20 仙界神珠 LV1
1/15 守护神珠 LV1
1/15 制魔神珠 LV1
1/15 强化神珠 LV1
1/20 中毒神珠 LV1
1/20 酷寒神珠 LV1
1/25 集中神珠 LV1
1/25 疾风神珠 LV1
1/20 忍耐神珠 LV1


;Books
;;Warrior
1/200 攻破斩 LV1
1/400 护身气幕 LV1
1/500 剑气爆 LV1
1/600 反击 LV1

;;Wizard
1/200 分身术 LV1
1/400 流星火雨 LV1
1/500 深延术 LV1
1/600 冰焰术 LV1
1/700 LavaKing LV1
1/850 FrozenRains LV1

;;Taoist
1/400 复活术 LV1
1/400 毒云 LV1
1/500 瘟疫 LV1
1/700 血龙水 LV1
1/850 HeadShot LV1
1/1000 HolyShield LV1

;;Assassion
1/200 轻身步 LV1
1/400 烈火身 LV1
1/600 血风击 LV1
1/700 月华乱舞 LV1
1/850 ShadowStep LV1
1/900 FuryWaves LV1

;Archer
1/200 毒药射击 LV1
1/400 削弱射击 LV1
1/600 召唤蛇 LV1
1/110 汽油射击 LV1
1/900 融为一体 LV1


;CraftingMaterials


;Quests
 LV1


;Level 2
;Gold
1/1 Gold 25000 LV2

;Pots + Other
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 太阳水 LV2
1/1 祝福油 LV2
1/10 GoldenBenedictionOil LV2


1/10 觉醒之魂0 LV2
1/20 觉醒之魂0 LV2

1/150 OrkMohzatRune LV2

;Weapons
1/250 开天 LV2
1/250 力量之弓 LV2
1/250 镇天 LV2
1/250 玄天 LV2
1/650 月轮剑 LV2
1/650 恐怖弓 LV2
1/650 月轮枪 LV2
1/650 变种剑 LV2
1/750 黑虎斧 LV2
1/750 致命弓 LV2
1/750 火莲杖 LV2
1/750 鹤羽扇 LV2
1/1150 碧血混水剑 LV2
1/1150 虹玄混地剑 LV2
1/1150 阿普斯弓 LV2
1/1150 翊仙混真扇 LV2

;Armour
1/300 黑虎甲胄(男) LV2
1/300 黑虎甲胄(女) LV2
1/300 莲盛魔衣(男) LV2
1/300 莲盛魔衣(女) LV2
1/300 鹤羽圣衣(男) LV2
1/300 鹤羽圣衣(女) LV2
1/450 赤之魔甲(男) LV2
1/450 赤之魔甲(女) LV2
1/450 青之魔甲(男) LV2
1/450 青之魔甲(女) LV2
1/450 绿之魔甲(男) LV2
1/450 绿之魔甲(女) LV2
1/750 WatcherSuit(M) LV2
1/750 WatcherSuit(F) LV2
1/750 鳞甲(男) LV2
1/750 鳞甲(女) LV2

;Upgrade Armour
1/4500 天衣无缝(男) LV2
1/4500 天衣无缝(女) LV2
1/4500 HeavenRedArmour(M) LV2
1/4500 HeavenRedArmour(F) LV2
1/4500 HeavenGrnArmour(M) LV2
1/4500 HeavenGrnArmour(F) LV2

;Helmets
1/40 英雄头盔 LV2
1/40 贤者帽 LV2
1/40 通天冠 LV2
1/40 修罗头盔 LV2
1/250 贝玉头盔 LV2
1/260 PuiYuHelmet LV2
1/570 WatcherHelmet LV2

;Necklace
1/50 破角 LV2
1/50 针坠 LV2
1/50 心如明镜 LV2
1/275 曜紫天坠 LV2
1/275 秀女十字盘 LV2
1/275 金刚陶坠 LV2
1/500 DefenderNecklace LV2

;Bracelets
1/50 铁魔双轮 LV2
1/50 玉女天使 LV2
1/50 曜火虹轮 LV2
1/270 暴虎归山 LV2
1/275 曜宝探魔轮 LV2
1/275 五行悟思轮 LV2
1/575 WatcherGloves LV2
1/500 DefenderGlove LV2

;Rings
1/50 盟约戒指 LV2
1/50 五行连环 LV2
1/50 血玉环 LV2
1/275 飞曜环 LV2
1/275 金刚伏魔环 LV2
1/275 金龙戒指 LV2
1/500 DefenderRing LV2

;Shoes
1/550 WatcherBoots LV2

;Belts


;Medals


;Shields
1/700 OrcsShield LV2

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV2
1/10 魔性宝玉  LV2
1/10 仙界宝玉 LV2
1/15 守护宝玉 LV2
1/15 制魔宝玉 LV2
1/15 强化宝玉 LV2

1/30 中毒宝玉 LV2
1/32 酷寒宝玉 LV2
1/35 集中宝玉 LV2
1/35 疾风宝玉 LV2
1/40 忍耐宝玉 LV2

1/20 勇猛神珠 LV2
1/20 魔性神珠 LV2
1/20 仙界神珠 LV2
1/15 守护神珠 LV2
1/15 制魔神珠 LV2
1/15 强化神珠 LV2
1/20 中毒神珠 LV2
1/20 酷寒神珠 LV2
1/25 集中神珠 LV2
1/25 疾风神珠 LV2
1/20 忍耐神珠 LV2


;Books
;;Warrior
1/200 攻破斩 LV2
1/400 护身气幕 LV2
1/500 剑气爆 LV2
1/600 反击 LV2

;;Wizard
1/200 分身术 LV2
1/400 流星火雨 LV2
1/500 深延术 LV2
1/600 冰焰术 LV2
1/700 LavaKing LV2
1/850 FrozenRains LV2

;;Taoist
1/400 复活术 LV2
1/400 毒云 LV2
1/500 瘟疫 LV2
1/700 血龙水 LV2
1/850 HeadShot LV2
1/1000 HolyShield LV2

;;Assassion
1/200 轻身步 LV2
1/400 烈火身 LV2
1/600 血风击 LV2
1/700 月华乱舞 LV2
1/850 ShadowStep LV2
1/900 FuryWaves LV2

;Archer
1/200 毒药射击 LV2
1/400 削弱射击 LV2
1/600 召唤蛇 LV2
1/110 汽油射击 LV2
1/900 融为一体 LV2

;CraftingMaterials


;Quests
 LV2


;Level 3
;Gold
1/1 Gold 25000 LV3

;Pots + Other
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 太阳水 LV3
1/1 祝福油 LV3
1/10 GoldenBenedictionOil LV3


1/10 觉醒之魂0 LV3
1/20 觉醒之魂0 LV3

1/150 OrkMohzatRune LV3

;Weapons
1/250 开天 LV3
1/250 力量之弓 LV3
1/250 镇天 LV3
1/250 玄天 LV3
1/650 月轮剑 LV3
1/650 恐怖弓 LV3
1/650 月轮枪 LV3
1/650 变种剑 LV3
1/750 黑虎斧 LV3
1/750 致命弓 LV3
1/750 火莲杖 LV3
1/750 鹤羽扇 LV3
1/1150 碧血混水剑 LV3
1/1150 虹玄混地剑 LV3
1/1150 翊仙混真扇 LV3
1/1150 阿普斯弓 LV3

;Armour
1/300 黑虎甲胄(男) LV3
1/300 黑虎甲胄(女) LV3
1/300 莲盛魔衣(男) LV3
1/300 莲盛魔衣(女) LV3
1/300 鹤羽圣衣(男) LV3
1/300 鹤羽圣衣(女) LV3
1/450 赤之魔甲(男) LV3
1/450 赤之魔甲(女) LV3
1/450 青之魔甲(男) LV3
1/450 青之魔甲(女) LV3
1/450 绿之魔甲(男) LV3
1/450 绿之魔甲(女) LV3
1/750 WatcherSuit(M) LV3
1/750 WatcherSuit(F) LV3
1/750 鳞甲(男) LV3
1/750 鳞甲(女) LV3

;Upgrade Armour
1/4500 天衣无缝(男) LV3
1/4500 天衣无缝(女) LV3
1/4500 HeavenRedArmour(M) LV3
1/4500 HeavenRedArmour(F) LV3
1/4500 HeavenGrnArmour(M) LV3
1/4500 HeavenGrnArmour(F) LV3

;Helmets
1/40 英雄头盔 LV3
1/40 贤者帽 LV3
1/40 通天冠 LV3
1/40 修罗头盔 LV3
1/250 贝玉头盔 LV3
1/260 PuiYuHelmet LV3
1/570 WatcherHelmet LV3

;Necklace
1/50 破角 LV3
1/50 针坠 LV3
1/50 心如明镜 LV3
1/275 曜紫天坠 LV3
1/275 秀女十字盘 LV3
1/275 金刚陶坠 LV3
1/500 DefenderNecklace LV3

;Bracelets
1/50 铁魔双轮 LV3
1/50 玉女天使 LV3
1/50 曜火虹轮 LV3
1/270 暴虎归山 LV3
1/275 曜宝探魔轮 LV3
1/275 五行悟思轮 LV3
1/575 WatcherGloves LV3
1/500 DefenderGlove LV3

;Rings
1/50 盟约戒指 LV3
1/50 五行连环 LV3
1/50 血玉环 LV3
1/275 飞曜环 LV3
1/275 金刚伏魔环 LV3
1/275 金龙戒指 LV3
1/500 DefenderRing LV3

;Shoes
1/550 WatcherBoots LV3

;Belts


;Medals


;Shields
1/700 OrcsShield LV3

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV3
1/10 魔性宝玉  LV3
1/10 仙界宝玉 LV3
1/15 守护宝玉 LV3
1/15 制魔宝玉 LV3
1/15 强化宝玉 LV3

1/30 中毒宝玉 LV3
1/32 酷寒宝玉 LV3
1/35 集中宝玉 LV3
1/35 疾风宝玉 LV3
1/40 忍耐宝玉 LV3

1/20 勇猛神珠 LV3
1/20 魔性神珠 LV3
1/20 仙界神珠 LV3
1/15 守护神珠 LV3
1/15 制魔神珠 LV3
1/15 强化神珠 LV3
1/20 中毒神珠 LV3
1/20 酷寒神珠 LV3
1/25 集中神珠 LV3
1/25 疾风神珠 LV3
1/20 忍耐神珠 LV3


;Books
;;Warrior
1/200 攻破斩 LV3
1/400 护身气幕 LV3
1/500 剑气爆 LV3
1/600 反击 LV3

;;Wizard
1/200 分身术 LV3
1/400 流星火雨 LV3
1/500 深延术 LV3
1/600 冰焰术 LV3
1/700 LavaKing LV3
1/850 FrozenRains LV3

;;Taoist
1/400 复活术 LV3
1/400 毒云 LV3
1/500 瘟疫 LV3
1/700 血龙水 LV3
1/850 HeadShot LV3
1/1000 HolyShield LV3

;;Assassion
1/200 轻身步 LV3
1/400 烈火身 LV3
1/600 血风击 LV3
1/700 月华乱舞 LV3
1/850 ShadowStep LV3
1/900 FuryWaves LV3

;Archer
1/200 毒药射击 LV3
1/400 削弱射击 LV3
1/600 召唤蛇 LV3
1/110 汽油射击 LV3
1/900 融为一体 LV3

;CraftingMaterials


;Quests
 LV3


;Level 4
;Gold
1/1 Gold 25000 LV4

;Pots + Other
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 太阳水 LV4
1/1 祝福油 LV4
1/10 GoldenBenedictionOil LV4


1/10 觉醒之魂0 LV4
1/20 觉醒之魂0 LV4

1/150 OrkMohzatRune LV4

;Weapons
1/250 开天 LV4
1/250 力量之弓 LV4
1/250 镇天 LV4
1/250 玄天 LV4
1/650 月轮剑 LV4
1/650 恐怖弓 LV4
1/650 月轮枪 LV4
1/650 变种剑 LV4
1/750 黑虎斧 LV4
1/750 致命弓 LV4
1/750 火莲杖 LV4
1/750 鹤羽扇 LV4
1/1150 碧血混水剑 LV4
1/1150 虹玄混地剑 LV4
1/1150 阿普斯弓 LV4
1/1150 翊仙混真扇 LV4

;Armour
1/300 黑虎甲胄(男) LV4
1/300 黑虎甲胄(女) LV4
1/300 莲盛魔衣(男) LV4
1/300 莲盛魔衣(女) LV4
1/300 鹤羽圣衣(男) LV4
1/300 鹤羽圣衣(女) LV4
1/450 赤之魔甲(男) LV4
1/450 赤之魔甲(女) LV4
1/450 青之魔甲(男) LV4
1/450 青之魔甲(女) LV4
1/450 绿之魔甲(男) LV4
1/450 绿之魔甲(女) LV4
1/750 WatcherSuit(M) LV4
1/750 WatcherSuit(F) LV4
1/750 鳞甲(男) LV4
1/750 鳞甲(女) LV4

;Upgrade Armour
1/4500 天衣无缝(男) LV4
1/4500 天衣无缝(女) LV4
1/4500 HeavenRedArmour(M) LV4
1/4500 HeavenRedArmour(F) LV4
1/4500 HeavenGrnArmour(M) LV4
1/4500 HeavenGrnArmour(F) LV4

;Helmets
1/40 英雄头盔 LV4
1/40 贤者帽 LV4
1/40 通天冠 LV4
1/40 修罗头盔 LV4
1/250 贝玉头盔 LV4
1/260 PuiYuHelmet LV4
1/570 WatcherHelmet LV4

;Necklace
1/50 破角 LV4
1/50 针坠 LV4
1/50 心如明镜 LV4
1/275 曜紫天坠 LV4
1/275 秀女十字盘 LV4
1/275 金刚陶坠 LV4
1/500 DefenderNecklace LV4

;Bracelets
1/50 铁魔双轮 LV4
1/50 玉女天使 LV4
1/50 曜火虹轮 LV4
1/270 暴虎归山 LV4
1/275 曜宝探魔轮 LV4
1/275 五行悟思轮 LV4
1/575 WatcherGloves LV4
1/500 DefenderGlove LV4

;Rings
1/50 盟约戒指 LV4
1/50 五行连环 LV4
1/50 血玉环 LV4
1/275 飞曜环 LV4
1/275 金刚伏魔环 LV4
1/275 金龙戒指 LV4
1/500 DefenderRing LV4

;Shoes
1/550 WatcherBoots LV4

;Belts


;Medals


;Shields
1/700 OrcsShield LV4

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV4
1/10 魔性宝玉  LV4
1/10 仙界宝玉 LV4
1/15 守护宝玉 LV4
1/15 制魔宝玉 LV4
1/15 强化宝玉 LV4

1/30 中毒宝玉 LV4
1/32 酷寒宝玉 LV4
1/35 集中宝玉 LV4
1/35 疾风宝玉 LV4
1/40 忍耐宝玉 LV4

1/20 勇猛神珠 LV4
1/20 魔性神珠 LV4
1/20 仙界神珠 LV4
1/15 守护神珠 LV4
1/15 制魔神珠 LV4
1/15 强化神珠 LV4
1/20 中毒神珠 LV4
1/20 酷寒神珠 LV4
1/25 集中神珠 LV4
1/25 疾风神珠 LV4
1/20 忍耐神珠 LV4


;Books
;;Warrior
1/200 攻破斩 LV4
1/400 护身气幕 LV4
1/500 剑气爆 LV4
1/600 反击 LV4

;;Wizard
1/200 分身术 LV4
1/400 流星火雨 LV4
1/500 深延术 LV4
1/600 冰焰术 LV4
1/700 LavaKing LV4
1/850 FrozenRains LV4

;;Taoist
1/400 复活术 LV4
1/400 毒云 LV4
1/500 瘟疫 LV4
1/700 血龙水 LV4
1/850 HeadShot LV4
1/1000 HolyShield LV4

;;Assassion
1/200 轻身步 LV4
1/400 烈火身 LV4
1/600 血风击 LV4
1/700 月华乱舞 LV4
1/850 ShadowStep LV4
1/900 FuryWaves LV4

;Archer
1/200 毒药射击 LV4
1/400 削弱射击 LV4
1/600 召唤蛇 LV4
1/110 汽油射击 LV4
1/900 融为一体 LV4

;CraftingMaterials


;Quests
 LV4


;Level 5
;Gold
1/1 Gold 25000 LV5

;Pots + Other
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 太阳水 LV5
1/1 祝福油 LV5
1/10 GoldenBenedictionOil LV5

1/10 觉醒之魂0 LV5
1/20 觉醒之魂0 LV5

1/150 OrkMohzatRune LV5

;Weapons
1/250 开天 LV5
1/250 力量之弓 LV5
1/250 镇天 LV5
1/250 玄天 LV5
1/650 月轮剑 LV5
1/650 恐怖弓 LV5
1/650 月轮枪 LV5
1/650 变种剑 LV5
1/750 黑虎斧 LV5
1/750 致命弓 LV5
1/750 火莲杖 LV5
1/750 鹤羽扇 LV5
1/1150 碧血混水剑 LV5
1/1150 虹玄混地剑 LV5
1/1150 阿普斯弓 LV5
1/1150 翊仙混真扇 LV5

;Armour
1/300 黑虎甲胄(男) LV5
1/300 黑虎甲胄(女) LV5
1/300 莲盛魔衣(男) LV5
1/300 莲盛魔衣(女) LV5
1/300 鹤羽圣衣(男) LV5
1/300 鹤羽圣衣(女) LV5
1/450 赤之魔甲(男) LV5
1/450 赤之魔甲(女) LV5
1/450 青之魔甲(男) LV5
1/450 青之魔甲(女) LV5
1/450 绿之魔甲(男) LV5
1/450 绿之魔甲(女) LV5
1/750 WatcherSuit(M) LV5
1/750 WatcherSuit(F) LV5
1/750 鳞甲(男) LV5
1/750 鳞甲(女) LV5

;Upgrade Armour
1/4500 天衣无缝(男) LV5
1/4500 天衣无缝(女) LV5
1/4500 HeavenRedArmour(M) LV5
1/4500 HeavenRedArmour(F) LV5
1/4500 HeavenGrnArmour(M) LV5
1/4500 HeavenGrnArmour(F) LV5

;Helmets
1/40 英雄头盔 LV5
1/40 贤者帽 LV5
1/40 通天冠 LV5
1/40 修罗头盔 LV5
1/250 贝玉头盔 LV5
1/260 PuiYuHelmet LV5
1/570 WatcherHelmet LV5

;Necklace
1/50 破角 LV5
1/50 针坠 LV5
1/50 心如明镜 LV5
1/275 曜紫天坠 LV5
1/275 秀女十字盘 LV5
1/275 金刚陶坠 LV5
1/500 DefenderNecklace LV5

;Bracelets
1/50 铁魔双轮 LV5
1/50 玉女天使 LV5
1/50 曜火虹轮 LV5
1/270 暴虎归山 LV5
1/275 曜宝探魔轮 LV5
1/275 五行悟思轮 LV5
1/575 WatcherGloves LV5
1/500 DefenderGlove LV5

;Rings
1/50 盟约戒指 LV5
1/50 五行连环 LV5
1/50 血玉环 LV5
1/275 飞曜环 LV5
1/275 金刚伏魔环 LV5
1/275 金龙戒指 LV5
1/500 DefenderRing LV5

;Shoes
1/550 WatcherBoots LV5

;Belts


;Medals


;Shields
1/700 OrcsShield LV5

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV5
1/10 魔性宝玉  LV5
1/10 仙界宝玉 LV5
1/15 守护宝玉 LV5
1/15 制魔宝玉 LV5
1/15 强化宝玉 LV5

1/30 中毒宝玉 LV5
1/32 酷寒宝玉 LV5
1/35 集中宝玉 LV5
1/35 疾风宝玉 LV5
1/40 忍耐宝玉 LV5

1/20 勇猛神珠 LV5
1/20 魔性神珠 LV5
1/20 仙界神珠 LV5
1/15 守护神珠 LV5
1/15 制魔神珠 LV5
1/15 强化神珠 LV5
1/20 中毒神珠 LV5
1/20 酷寒神珠 LV5
1/25 集中神珠 LV5
1/25 疾风神珠 LV5
1/20 忍耐神珠 LV5


;Books
;;Warrior
1/200 攻破斩 LV5
1/400 护身气幕 LV5
1/500 剑气爆 LV5
1/600 反击 LV5

;;Wizard
1/200 分身术 LV5
1/400 流星火雨 LV5
1/500 深延术 LV5
1/600 冰焰术 LV5
1/700 LavaKing LV5
1/850 FrozenRains LV5

;;Taoist
1/400 复活术 LV5
1/400 毒云 LV5
1/500 瘟疫 LV5
1/700 血龙水 LV5
1/850 HeadShot LV5
1/1000 HolyShield LV5

;;Assassion
1/200 轻身步 LV5
1/400 烈火身 LV5
1/600 血风击 LV5
1/700 月华乱舞 LV5
1/850 ShadowStep LV5
1/900 FuryWaves LV5

;Archer
1/200 毒药射击 LV5
1/400 削弱射击 LV5
1/600 召唤蛇 LV5
1/110 汽油射击 LV5
1/900 融为一体 LV5

;CraftingMaterials


;Quests
 LV5


;Level 6
;Gold
1/1 Gold 25000 LV6

;Pots + Other
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 太阳水 LV6
1/1 祝福油 LV6
1/10 GoldenBenedictionOil LV6


1/10 觉醒之魂0 LV6
1/20 觉醒之魂0 LV6

1/150 OrkMohzatRune LV6

;Weapons
1/250 开天 LV6
1/250 力量之弓 LV6
1/250 镇天 LV6
1/250 玄天 LV6
1/650 月轮剑 LV6
1/650 恐怖弓 LV6
1/650 月轮枪 LV6
1/650 变种剑 LV6
1/750 黑虎斧 LV6
1/750 致命弓 LV6
1/750 火莲杖 LV6
1/750 鹤羽扇 LV6
1/1150 碧血混水剑 LV6
1/1150 虹玄混地剑 LV6
1/1150 阿普斯弓 LV6
1/1150 翊仙混真扇 LV6

;Armour
1/300 黑虎甲胄(男) LV6
1/300 黑虎甲胄(女) LV6
1/300 莲盛魔衣(男) LV6
1/300 莲盛魔衣(女) LV6
1/300 鹤羽圣衣(男) LV6
1/300 鹤羽圣衣(女) LV6
1/450 赤之魔甲(男) LV6
1/450 赤之魔甲(女) LV6
1/450 青之魔甲(男) LV6
1/450 青之魔甲(女) LV6
1/450 绿之魔甲(男) LV6
1/450 绿之魔甲(女) LV6
1/750 WatcherSuit(M) LV6
1/750 WatcherSuit(F) LV6
1/750 鳞甲(男) LV6
1/750 鳞甲(女) LV6

;Upgrade Armour
1/4500 天衣无缝(男) LV6
1/4500 天衣无缝(女) LV6
1/4500 HeavenRedArmour(M) LV6
1/4500 HeavenRedArmour(F) LV6
1/4500 HeavenGrnArmour(M) LV6
1/4500 HeavenGrnArmour(F) LV6

;Helmets
1/40 英雄头盔 LV6
1/40 贤者帽 LV6
1/40 通天冠 LV6
1/40 修罗头盔 LV6
1/250 贝玉头盔 LV6
1/260 PuiYuHelmet LV6
1/570 WatcherHelmet LV6

;Necklace
1/50 破角 LV6
1/50 针坠 LV6
1/50 心如明镜 LV6
1/275 曜紫天坠 LV6
1/275 秀女十字盘 LV6
1/275 金刚陶坠 LV6
1/500 DefenderNecklace LV6

;Bracelets
1/50 铁魔双轮 LV6
1/50 玉女天使 LV6
1/50 曜火虹轮 LV6
1/270 暴虎归山 LV6
1/275 曜宝探魔轮 LV6
1/275 五行悟思轮 LV6
1/575 WatcherGloves LV6
1/500 DefenderGlove LV6

;Rings
1/50 盟约戒指 LV6
1/50 五行连环 LV6
1/50 血玉环 LV6
1/275 飞曜环 LV6
1/275 金刚伏魔环 LV6
1/275 金龙戒指 LV6
1/500 DefenderRing LV6

;Shoes
1/550 WatcherBoots LV6

;Belts


;Medals


;Shields
1/700 OrcsShield LV6

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV6
1/10 魔性宝玉  LV6
1/10 仙界宝玉 LV6
1/15 守护宝玉 LV6
1/15 制魔宝玉 LV6
1/15 强化宝玉 LV6

1/30 中毒宝玉 LV6
1/32 酷寒宝玉 LV6
1/35 集中宝玉 LV6
1/35 疾风宝玉 LV6
1/40 忍耐宝玉 LV6

1/20 勇猛神珠 LV6
1/20 魔性神珠 LV6
1/20 仙界神珠 LV6
1/15 守护神珠 LV6
1/15 制魔神珠 LV6
1/15 强化神珠 LV6
1/20 中毒神珠 LV6
1/20 酷寒神珠 LV6
1/25 集中神珠 LV6
1/25 疾风神珠 LV6
1/20 忍耐神珠 LV6


;Books
;;Warrior
1/200 攻破斩 LV6
1/400 护身气幕 LV6
1/500 剑气爆 LV6
1/600 反击 LV6

;;Wizard
1/200 分身术 LV6
1/400 流星火雨 LV6
1/500 深延术 LV6
1/600 冰焰术 LV6
1/700 LavaKing LV6
1/850 FrozenRains LV6

;;Taoist
1/400 复活术 LV6
1/400 毒云 LV6
1/500 瘟疫 LV6
1/700 血龙水 LV6
1/850 HeadShot LV6
1/1000 HolyShield LV6

;;Assassion
1/200 轻身步 LV6
1/400 烈火身 LV6
1/600 血风击 LV6
1/700 月华乱舞 LV6
1/850 ShadowStep LV6
1/900 FuryWaves LV6

;Archer
1/200 毒药射击 LV6
1/400 削弱射击 LV6
1/600 召唤蛇 LV6
1/110 汽油射击 LV6
1/900 融为一体 LV6

;CraftingMaterials


;Quests
 LV6


;Level 7
;Gold
1/1 Gold 25000 LV7

;Pots + Other
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 太阳水 LV7
1/1 祝福油 LV7
1/10 GoldenBenedictionOil LV7


1/10 觉醒之魂0 LV7
1/20 觉醒之魂0 LV7

1/150 OrkMohzatRune LV7

;Weapons
1/250 开天 LV7
1/250 力量之弓 LV7
1/250 镇天 LV7
1/250 玄天 LV7
1/650 月轮剑 LV7
1/650 恐怖弓 LV7
1/650 月轮枪 LV7
1/650 变种剑 LV7
1/750 黑虎斧 LV7
1/750 致命弓 LV7
1/750 火莲杖 LV7
1/750 鹤羽扇 LV7
1/1150 碧血混水剑 LV7
1/1150 虹玄混地剑 LV7
1/1150 阿普斯弓 LV7
1/1150 翊仙混真扇 LV7

;Armour
1/300 黑虎甲胄(男) LV7
1/300 黑虎甲胄(女) LV7
1/300 莲盛魔衣(男) LV7
1/300 莲盛魔衣(女) LV7
1/300 鹤羽圣衣(男) LV7
1/300 鹤羽圣衣(女) LV7
1/450 赤之魔甲(男) LV7
1/450 赤之魔甲(女) LV7
1/450 青之魔甲(男) LV7
1/450 青之魔甲(女) LV7
1/450 绿之魔甲(男) LV7
1/450 绿之魔甲(女) LV7
1/750 WatcherSuit(M) LV7
1/750 WatcherSuit(F) LV7
1/750 鳞甲(男) LV7
1/750 鳞甲(女) LV7

;Upgrade Armour
1/4500 天衣无缝(男) LV7
1/4500 天衣无缝(女) LV7
1/4500 HeavenRedArmour(M) LV7
1/4500 HeavenRedArmour(F) LV7
1/4500 HeavenGrnArmour(M) LV7
1/4500 HeavenGrnArmour(F) LV7

;Helmets
1/40 英雄头盔 LV7
1/40 贤者帽 LV7
1/40 通天冠 LV7
1/40 修罗头盔 LV7
1/250 贝玉头盔 LV7
1/260 PuiYuHelmet LV7
1/570 WatcherHelmet LV7

;Necklace
1/50 破角 LV7
1/50 针坠 LV7
1/50 心如明镜 LV7
1/275 曜紫天坠 LV7
1/275 秀女十字盘 LV7
1/275 金刚陶坠 LV7
1/500 DefenderNecklace LV7

;Bracelets
1/50 铁魔双轮 LV7
1/50 玉女天使 LV7
1/50 曜火虹轮 LV7
1/270 暴虎归山 LV7
1/275 曜宝探魔轮 LV7
1/275 五行悟思轮 LV7
1/575 WatcherGloves LV7
1/500 DefenderGlove LV7

;Rings
1/50 盟约戒指 LV7
1/50 五行连环 LV7
1/50 血玉环 LV7
1/275 飞曜环 LV7
1/275 金刚伏魔环 LV7
1/275 金龙戒指 LV7
1/500 DefenderRing LV7

;Shoes
1/550 WatcherBoots LV7

;Belts


;Medals


;Shields
1/700 OrcsShield LV7

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV7
1/10 魔性宝玉  LV7
1/10 仙界宝玉 LV7
1/15 守护宝玉 LV7
1/15 制魔宝玉 LV7
1/15 强化宝玉 LV7

1/30 中毒宝玉 LV7
1/32 酷寒宝玉 LV7
1/35 集中宝玉 LV7
1/35 疾风宝玉 LV7
1/40 忍耐宝玉 LV7

1/20 勇猛神珠 LV7
1/20 魔性神珠 LV7
1/20 仙界神珠 LV7
1/15 守护神珠 LV7
1/15 制魔神珠 LV7
1/15 强化神珠 LV7
1/20 中毒神珠 LV7
1/20 酷寒神珠 LV7
1/25 集中神珠 LV7
1/25 疾风神珠 LV7
1/20 忍耐神珠 LV7


;Books
;;Warrior
1/200 攻破斩 LV7
1/400 护身气幕 LV7
1/500 剑气爆 LV7
1/600 反击 LV7

;;Wizard
1/200 分身术 LV7
1/400 流星火雨 LV7
1/500 深延术 LV7
1/600 冰焰术 LV7
1/700 LavaKing LV7
1/850 FrozenRains LV7

;;Taoist
1/400 复活术 LV7
1/400 毒云 LV7
1/500 瘟疫 LV7
1/700 血龙水 LV7
1/850 HeadShot LV7
1/1000 HolyShield LV7

;;Assassion
1/200 轻身步 LV7
1/400 烈火身 LV7
1/600 血风击 LV7
1/700 月华乱舞 LV7
1/850 ShadowStep LV7
1/900 FuryWaves LV7

;Archer
1/200 毒药射击 LV7
1/400 削弱射击 LV7
1/600 召唤蛇 LV7
1/110 汽油射击 LV7
1/900 融为一体 LV7

;CraftingMaterials


;Quests
 LV7


;Level 8
;Gold
1/1 Gold 25000 LV8

;Pots + Other
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 太阳水 LV8
1/1 祝福油 LV8
1/10 GoldenBenedictionOil LV8

1/10 觉醒之魂0 LV8
1/20 觉醒之魂0 LV8

1/150 OrkMohzatRune LV8

;Weapons
1/250 开天 LV8
1/250 力量之弓 LV8
1/250 镇天 LV8
1/250 玄天 LV8
1/650 月轮剑 LV8
1/650 恐怖弓 LV8
1/650 月轮枪 LV8
1/650 变种剑 LV8
1/750 黑虎斧 LV8
1/750 致命弓 LV8
1/750 火莲杖 LV8
1/750 鹤羽扇 LV8
1/1150 碧血混水剑 LV8
1/1150 阿普斯弓 LV8
1/1150 虹玄混地剑 LV8
1/1150 翊仙混真扇 LV8

;Armour
1/300 黑虎甲胄(男) LV8
1/300 黑虎甲胄(女) LV8
1/300 莲盛魔衣(男) LV8
1/300 莲盛魔衣(女) LV8
1/300 鹤羽圣衣(男) LV8
1/300 鹤羽圣衣(女) LV8
1/450 赤之魔甲(男) LV8
1/450 赤之魔甲(女) LV8
1/450 青之魔甲(男) LV8
1/450 青之魔甲(女) LV8
1/450 绿之魔甲(男) LV8
1/450 绿之魔甲(女) LV8
1/750 WatcherSuit(M) LV8
1/750 WatcherSuit(F) LV8
1/750 鳞甲(男) LV8
1/750 鳞甲(女) LV8

;Upgrade Armour
1/4500 天衣无缝(男) LV8
1/4500 天衣无缝(女) LV8
1/4500 HeavenRedArmour(M) LV8
1/4500 HeavenRedArmour(F) LV8
1/4500 HeavenGrnArmour(M) LV8
1/4500 HeavenGrnArmour(F) LV8

;Helmets
1/40 英雄头盔 LV8
1/40 贤者帽 LV8
1/40 通天冠 LV8
1/40 修罗头盔 LV8
1/250 贝玉头盔 LV8
1/260 PuiYuHelmet LV8
1/570 WatcherHelmet LV8

;Necklace
1/50 破角 LV8
1/50 针坠 LV8
1/50 心如明镜 LV8
1/275 曜紫天坠 LV8
1/275 秀女十字盘 LV8
1/275 金刚陶坠 LV8
1/500 DefenderNecklace LV8

;Bracelets
1/50 铁魔双轮 LV8
1/50 玉女天使 LV8
1/50 曜火虹轮 LV8
1/270 暴虎归山 LV8
1/275 曜宝探魔轮 LV8
1/275 五行悟思轮 LV8
1/575 WatcherGloves LV8
1/500 DefenderGlove LV8

;Rings
1/50 盟约戒指 LV8
1/50 五行连环 LV8
1/50 血玉环 LV8
1/275 飞曜环 LV8
1/275 金刚伏魔环 LV8
1/275 金龙戒指 LV8
1/500 DefenderRing LV8

;Shoes
1/550 WatcherBoots LV8

;Belts


;Medals


;Shields
1/700 OrcsShield LV8

;Pads


;Stone


;Gems/Orbs
1/10 勇猛宝玉 LV8
1/10 魔性宝玉  LV8
1/10 仙界宝玉 LV8
1/15 守护宝玉 LV8
1/15 制魔宝玉 LV8
1/15 强化宝玉 LV8

1/30 中毒宝玉 LV8
1/32 酷寒宝玉 LV8
1/35 集中宝玉 LV8
1/35 疾风宝玉 LV8
1/40 忍耐宝玉 LV8

1/20 勇猛神珠 LV8
1/20 魔性神珠 LV8
1/20 仙界神珠 LV8
1/15 守护神珠 LV8
1/15 制魔神珠 LV8
1/15 强化神珠 LV8
1/20 中毒神珠 LV8
1/20 酷寒神珠 LV8
1/25 集中神珠 LV8
1/25 疾风神珠 LV8
1/20 忍耐神珠 LV8


;Books
;;Warrior
1/200 攻破斩 LV8
1/400 护身气幕 LV8
1/500 剑气爆 LV8
1/600 反击 LV8

;;Wizard
1/200 分身术 LV8
1/400 流星火雨 LV8
1/500 深延术 LV8
1/600 冰焰术 LV8
1/700 LavaKing LV8
1/850 FrozenRains LV8

;;Taoist
1/400 复活术 LV8
1/400 毒云 LV8
1/500 瘟疫 LV8
1/700 血龙水 LV8
1/850 HeadShot LV8
1/1000 HolyShield LV8

;;Assassion
1/200 轻身步 LV8
1/400 烈火身 LV8
1/600 血风击 LV8
1/700 月华乱舞 LV8
1/850 ShadowStep LV8
1/900 FuryWaves LV8

;Archer
1/200 毒药射击 LV8
1/400 削弱射击 LV8
1/600 召唤蛇 LV8
1/110 汽油射击 LV8
1/900 融为一体 LV8

;CraftingMaterials


;Quests
 LV8
