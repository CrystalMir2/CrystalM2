;Gold
1/1 Gold 3000

;Potions
1/8 太阳水
1/8 太阳水

1/4000 贝玉腰带
1/4000 贝玉靴子
1/4000 贝玉头盔
1/6000 PuiYuHelmet

;Stone
1/940 HolyMcStone
1/940 HolyDcStone
1/940 HolyScStone

;Rings
1/3452 BlizzardRing
1/3452 MistyRing
1/3452 SmasherRing
1/3453 AscendedRing

;Boots

;Belt
1/3352 黑术腰
1/3353 AscendedBelt
1/3355 StrapofTrolls

1/1125 BraveryFragment
1/1125 MagicFragment
1/1125 SoulFragment
1/1125 ProtectionFragment
1/1125 EvilSlayerFragment
1/1125 DurabilityFragment
1/1125 StormFragment
1/1125 DisillusionFragment
1/1125 AccuracyFragment
1/1125 PoisonFragment
1/1125 FreezingFragment
1/1125 AgilityFragment
1/1125 EnduranceFragment
1/1130 寒冰珠
1/1900 盔甲铸造工具 
1/1900 TranslucentCrystal
1/1900 盔甲之书(勇猛)
1/1900 盔甲之书(仙术)
1/1900 盔甲之书(魔界)
1/1900 锈蚀的古代盔甲
1/1900 精神之书
1/1900 浩劫水晶