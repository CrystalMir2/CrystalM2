;Gold
1/12 Gold 600

;Pots + Other
1/30 金疮药(中量)
1/30 魔法药(中量)
1/70 金疮药(大量)
1/70 魔法药(大量)

;Weapons
1/4000 银弓
1/4000 炼狱
1/4000 魔杖
1/4000 银蛇
1/4000 双极刀

;Weapons Rare
1/2650 井中月
1/2650 血饮
1/7700 VanguardBlade
1/7700 VanguardStaff
1/7700 VanguardSerp
1/7700 VanguardBlades

;Armour
1/800 轻皮甲(男)
1/800 轻皮甲(女)
1/800 重盔甲(男) 
1/800 重盔甲(女) 
1/800 魔法长袍(男) 
1/800 魔法长袍(女) 
1/800 灵魂战衣(男) 
1/800 灵魂战衣(女) 
1/800 残影魔衣(男)
1/800 残影魔衣(女)

;Armour Rare
1/7900 VanguardArmour(M)
1/7900 VanguardArmour(F)


;Helmets Rare
1/6900 VanguardHelmet


;Necklace Rare
1/6900 VanguardNecklace


;Bracelets Rare
1/6900 VanguardWheel


;Rings Rare
1/6900 VanguardRing


;Shoes Rare
1/6900 VanguardBoots


;Belts Rare
1/6900 VanguardBelt


;Stone
1/600 生命守护石(中)
1/600 魔力守护石(中)
1/600 能力守护石(中)
1/600 攻击守护石(中)
1/600 魔法守护石(中)
1/600 道术守护石(中)
1/600 ACStone(M)
1/600 AMCStone(M)
1/600 DefenseStone(M)