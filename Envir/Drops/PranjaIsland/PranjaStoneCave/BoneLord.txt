;Gold
1/1 Gold 15000
1/100 金条

;Potions
1/1 金疮药(大量)
1/1 魔法药(大量)
1/1 金疮药(大量)
1/1 魔法药(大量)
1/2 金疮药(大量)
1/2 魔法药(大量)
1/2 金疮药(大量)
1/2 魔法药(大量)
1/1 金疮药(特大)
1/1 魔法药(特大)
1/2 金疮药(特大)
1/2 魔法药(特大)
1/1 太阳水
1/1 太阳水
1/1 强效太阳水
1/1 强效太阳水
1/2 太阳水
1/2 太阳水
1/2 强效太阳水
1/2 强效太阳水
1/3 祝福油
1/10 祝福油
1/5 战神油
1/30 道力药水(中)
1/30 魔法药水(中)
1/30 攻击药水(中)
1/30 疾风药水(中)

;Weapons
1/45 裁决之杖
1/45 骨玉权杖
1/45 无极棍
1/45 暗黑刀
1/45 魔弓
1/125 龙牙
1/125 怒斩
1/125 逍遥扇
1/125 冷月刀
1/125 轻弓
1/200 屠龙
1/200 噬魂法杖
1/200 龙纹剑
1/200 修罗刀
1/200 恶之弓
1/500 开天
1/500 镇天
1/500 玄天
1/500 暗真魔刀
1/500 力量之弓

;Armours
1/5 战神盔甲(男)
1/5 战神盔甲(女)
1/5 恶魔长袍(男)
1/5 恶魔长袍(女)
1/5 幽灵战衣(男)
1/5 幽灵战衣(女)
1/5 炎红战衣(男)
1/5 炎红战衣(女)
1/5 钢化盔甲(男)
1/5 钢化盔甲(女)
1/40 鬼面甲胄(男)
1/40 鬼面甲胄(女)
1/40 火龙魔衣(男)
1/40 火龙魔衣(女)
1/40 玄天宝衣(男)
1/40 玄天宝衣(女)
1/40 修罗血衣(男)
1/40 修罗血衣(女)
1/40 镶嵌盔甲(男)
1/40 镶嵌盔甲(女)
1/1800 天衣无缝(男)
1/1800 天衣无缝(女)

;Helmets
1/70 黑铁头盔
1/90 勇士头盔
1/150 英雄头盔
1/150 贤者帽
1/150 通天冠
1/150 修罗头盔
1/10 黄铜头盔
1/15 钢铁头盔
1/5 道士头盔
1/5 骷髅头盔

;Belts
1/20 铁腰带
1/30 青铜腰带
1/30 黄金腰带
1/60 金刚腰带

;Shoes
1/20 避魂靴
1/20 紫绸靴
1/30 龙靴
1/45 赤鳞靴

;Necklaces
1/80 狂风项链
1/80 绿色项链
1/80 恶魔铃铛
1/80 灵魂项链
1/45 幽灵项链
1/45 生命项链
1/45 天珠项链
1/5 灯笼项链
1/5 白色虎齿项链

;Rings
1/25 龙之戒指
1/25 红宝石戒指
1/25 铂金戒指
1/50 力量戒指
1/50 紫碧螺
1/50 泰坦戒指
1/100 奥玛环
1/100 鬼刃环
1/100 心意环
1/250 无极环
1/250 雷霆环
1/250 太极环
1/10 狂风戒指
1/5 降妖除魔戒指
1/5 珊瑚戒指
1/4000 护身戒指
1/4000 麻痹戒指

;Bracelets
1/75 骑士手镯
1/75 龙之手镯
1/75 心灵手镯
1/55 阎罗手套
1/30 思贝尔手镯
1/30 三眼手镯
1/100 抗魔轮
1/100 伏魔轮
1/100 八极轮
1/250 魂锁轮
1/250 太极轮
1/250 白驼手套

;Gems
1/300 酷寒神珠
1/300 强化神珠
1/300 勇猛神珠
1/300 魔性神珠
1/300 仙界神珠
1/300 守护神珠
1/300 制魔神珠
1/300 中毒神珠
1/300 集中神珠
1/300 回避神珠

;Books
1/35 冰咆哮
1/35 召唤神兽
1/35 烈火剑法
1/35 捕缚术
1/35 猛毒剑气
1/70 圆月弯刀
1/70 无极真气
1/70 灭天火
1/500 召唤月灵
1/600 分身术
1/100 攻破斩
1/100 月影术
1/100 吸气