[TOC]

----
### 配置文件说明(.txt)

#### ./Envir/MapInfo.txt

配置游戏中的地图,连接点,地图背景音乐,限制条件等,

---
#### ./Envir/MonGen.txt

游戏中的地图的刷怪配置,刷怪,间隔,数量

---
#### ./Envir/Npc.txt

游戏中的地图的NPC配置,NPC所处地图坐标,外形,对应的响应脚本

---
#### ./Envir/Notice.txt

游戏中公告,定时在游戏屏幕中间大字播报

---
#### ./Envir/LineMessage.txt

游戏中聊天框蓝底系统公告,一般用于展示游戏提示

---
#### ./Envir/DisabledChars.txt

游戏中屏蔽关键字,一行一个

---
#### ./Envir/NPCs/00Robot.txt
系统定时器配置脚本,可按时分秒定时循环执行脚本


> 关于注释说明性文字
> 以;开头,仅在.txt的配置文件有效,具体参考MapInfo.txt


---------------------------

### 目录说明


---
#### ./Envir/NPCs/

用于存放拓展游戏玩法的脚本,CrystalM2支持`JavaScript`语言编写的脚本,内置上百个函数,
脚本是由成百上千的自定义函数组成,文件名须以.js结尾
以下是对内置脚本文件的说明;

- Robot.js          - 系统定时器
- QFunction.js  - 系统触发
- *.js                    - NPC响应

##### ./Envir/NPCs/Robot.js
系统定时器函数,与之配套的文件./Envir/NPCs/00Robot.txt文件,

- 00Robot配置何时执行
- Robot.js配置具体执行的脚本代码
- Robot.js中的函数命名必须以Robot$开头

举例:
每5分钟给全服玩家问好

00Robot.txt:
```javascript
#AutoRun MIN 5 @HelloEveryOne
```

Robot.js:
```javascript
function Robot$HelloEveryOne() {
    ctx.sendMsg(" Are you ok ?",0)
    return ''
}
```


---
##### ./Envir/NPCs/QFunction.js
系统触发通知脚本, 比如:
1. 玩家被怪物攻击触发
2. 玩家死亡触发
3. 怪物死亡触发
4. 玩家穿戴装备触发
5. 玩家拾取触发
6. 玩家登录,升级触发
7. 技能释放,命中触发
8. 物品双击使用触发
9. 其他各种触发等等

QFunction.js配置具体执行的脚本代码,其函数命名必须以QFunction$开头

举例:
修改玩家拾取的装备的名字,添加*传说后缀

QFunction.js:
```javascript
function QFunction$PickUp() {
    //调用其他文件夹下的.js脚本文件
    return 拾取触发$拾取触发$Main(arguments[0],arguments[1],arguments[2],arguments[3]);
}
```


./Envir/Script/拾取触发/拾取触发.js:
```javascript
function 拾取触发$拾取触发$Main(id,postion,name,friendlyName) {
    var say = '';

    if(String.contains(friendlyName,"*")){
        var g = friendlyName.split('*');
        say += "[拾取触发] 该装备已经鉴定 :{"+g[1]+'#yellow}';
        say += '\r\n';
    } else {
        var newName = name+"*传说";
        ctx.changeItemName(id,newName);
        say += '[拾取触发] 恭喜,该装备鉴定为 :{' +newName +'#Red}';
        say += '\r\n';
    }
    say += String.format('[拾取触发]id: {0},位置: {1}, 名称: {2},改名: {3} \r\n',id,postion,name,friendlyName);
    return say;
}

```

> .js文件可以使用WebStorm软件编辑, 代码提示+自动补全+智能纠错. 会提示哪里写错了.出错的原因, 提高编辑效率

---
#### ./Envir/Drops

爆率掉落配置文件夹,以怪物名作为文件名, 配置每个怪物的掉落



#### ./Configs

##### AwakeningSystem.ini
装备觉醒配置



##### BaseStats*.ini

各个职业等级成长配置

- BaseStatsArcher.ini
- BaseStatsAssassin.ini
- BaseStatsTaoist.ini
- BaseStatsWarrior.ini
- BaseStatsWizard.ini

##### ExpList.ini
经验配置



##### FishingSystem.ini
钓鱼配置



##### GemSystem.ini
宝石配置



##### GoodsSystem.ini
NPC商店配置



##### Language.ini
语言配置



##### MailSystem.ini

邮件配置



##### MarriageSystem.ini
结婚配置



##### MentorSystem.ini
师徒配置



##### Mines.ini
挖矿配置



##### OrbsExpList.ini
矿石配置





##### RandomItemStats.ini
装备极品配置



##### RefineSystem.ini

 锻造配置



##### Setup.ini
服务端的系统配置文件,包含
- 网络端口,ip配置
- 安全区
- 备份,归档间隔
- 物品,宠物,召唤物等



##### WorldMap.ini
世界地图配置




### 数据库说明

#### ./ServerSqlite.db

服务端的系统静态数据文件, 为sqlite3格式的数据库文件,包含

- ItemInfo   装备道具
- MagicInfo   技能
- MonsterInfo  怪物
- GameShopItem 系统商铺

该文件为版本Mod制作的核心修改文件

#### ./AccountSqlite.db

服务端的玩家动态数据,为sqlite3格式的数据库文件,包含

- AccountInfo - 玩家账号信息
- CharacterInfo- 玩家角色信息
- FriendInfo 玩家好友信息
- Envir 服务器的一些动态数据
- AuctionInfo 拍卖行
- UserItem- 玩家物品,包含仓库,拍卖,装备,镶嵌,背包,详见`ItemPostionType`枚举
- UserItemCustomProperty 物品的自定义属性
- UserMagic 玩家技能
- UserIntelligentCreature 玩家宠物
- UserData 玩家持久化数据

ItemPostionType.cs
```c#
public enum ItemPostionType:int {
        ///地上
        System,
        /// 背包 1
        Inventory      ,
        /// 装备 2
        Equipment      ,
        ///交易栏 3
        Trade          ,
        /// 任务背包 4
        QuestInventory ,
        ///账号仓库 5
        Storage ,
        /// 锻造 6
        Refine         ,
        ///租赁 7
        RentedItems        ,
        ///租赁移除 8
        RentedItemsToRemove,
        ///拍卖 9
        Auction,
        ///拍卖 10
        Guild,
        ///嵌套 11
        Solt,
        ///邮件 12
        Mail,
}

```



>  该文件可以使用多种sql数据库编辑软件修改,如:Navicat Premium
>  可根据上述sqldb文件,可自行开发合区,可视化配置等工具软件



